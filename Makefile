#SILENCE?=@
PROJECT_NAME=lib-m5gm
BUILD_TIME=$(shell date +%G%m%d-%H%M%S)
VERSION=1.0.0
MAJOR_VERSION=$(word 1,$(subst ., ,${VERSION}))

PWD=$(shell pwd)
BUILD_PATH=$(PWD)/build
CFLAGS+=-std=gnu99 -I$(PWD)/include

# Back trace parameter define
BT_FLAGS = -funwind-tables

LIBRARY_NAME=libm5gm
LIBRARY_SO_NAME=$(LIBRARY_NAME).so.$(MAJOR_VERSION)
LIBRARY_FILE_NAME=$(LIBRARY_NAME).so.$(VERSION)

CPPUTEST_CFLAGS:=-DPOSIX -g -MMD -MP $(COMMON_INCLUDE_DIRS) \
        -include CppUTest/MemoryLeakDetectorMallocMacros.h \
        -fprofile-arcs -ftest-coverage
CPPUTEST_CXXFLAGS:=-DPOSIX -g -MMD -MP $(COMMON_INCLUDE_DIRS) \
        -include CppUTest/MemoryLeakDetectorMallocMacros.h \
        -fprofile-arcs -ftest-coverage
UNITTEST_OBJS:=$$(find $(BUILD_PATH)/unittest -type f -name *.o)
UNITTEST_SRC_OBJS:=$$(find $(BUILD_PATH)/unittest -type f -name *.o ! -name test_*.o -a ! -name mock_*.o ! -name main.o)
RESULT_ROOT:=$(abspath $(PWD)/result/)
TEST_OUTPUT_FILE:=$(RESULT_ROOT)/test_output.txt
GCOV_OUTPUT_FILE:=$(RESULT_ROOT)/gcov_output.txt
GCOV_ERROR_FILE:=$(RESULT_ROOT)/gcov_error.txt
TEST_REPORT_FILE:=$(RESULT_ROOT)/test_report.txt

FOR_BUILD_PATH=$(PWD)
LIB_DIR = $(TARGET_DIR)/lib/
INCLUDE_DIR = $(TARGET_DIR)/lib/include/
.PHONY: all install clean test
.SILENT: test
all:
	$(SILENCE)mkdir -p $(BUILD_PATH)

# Build library
	$(SILENCE)$(CC) $(CFLAGS) $(BT_FLAGS) -fPIC -c src/lib_m5gm.c -o $(BUILD_PATH)/lib_m5gm.o
	$(SILENCE)$(CC) $(CFLAGS) $(BT_FLAGS) -fPIC -c src/m5gm_popen.c -o $(BUILD_PATH)/m5gm_popen.o
	$(SILENCE)$(CC) $(CFLAGS) $(BT_FLAGS) -fPIC -c src/m5gm_event_callback.c -o $(BUILD_PATH)/m5gm_event_callback.o
	$(SILENCE)$(CC) $(CFLAGS) $(BT_FLAGS) -fPIC -c src/m5gm_g_link.c -o $(BUILD_PATH)/m5gm_g_link.o
	$(SILENCE)$(CC) $(CFLAGS) $(BT_FLAGS) -fPIC -c src/m5gm_log.c -o $(BUILD_PATH)/m5gm_log.o
	$(SILENCE)$(CC) $(CFLAGS) $(BT_FLAGS) -fPIC -c src/m5gm_lock.c -o $(BUILD_PATH)/m5gm_lock.o
	$(SILENCE)$(CC) $(CFLAGS) $(BT_FLAGS) -shared -Wl,-soname,$(LIBRARY_SO_NAME) \
		-o $(BUILD_PATH)/$(LIBRARY_FILE_NAME) \
		$(BUILD_PATH)/lib_m5gm.o \
		$(BUILD_PATH)/m5gm_popen.o \
		$(BUILD_PATH)/m5gm_log.o \
		$(BUILD_PATH)/m5gm_lock.o \
		$(BUILD_PATH)/m5gm_g_link.o \
		$(BUILD_PATH)/m5gm_event_callback.o
		
	$(SILENCE)ln -sf $(LIBRARY_FILE_NAME) $(BUILD_PATH)/$(LIBRARY_SO_NAME)
	$(SILENCE)ln -sf $(LIBRARY_SO_NAME) $(BUILD_PATH)/$(LIBRARY_NAME).so

# Build utility
	$(SILENCE)$(CC) $(CFLAGS) -o $(BUILD_PATH)/util_m5gm utility/util_m5gm.c \
		-L $(BUILD_PATH) -l m5gm -lpthread\
		-Wl,--unresolved-symbols=ignore-in-shared-libs
install:
	echo $(TARGET_DIR)
	$(SILENCE)mkdir -m 740 -p $(LIB_DIR)/
	$(SILENCE)cp -P $(BUILD_PATH)/$(LIBRARY_NAME).so* $(LIB_DIR)/

	$(SILENCE)mkdir -m 740 -p $(INCLUDE_DIR)/lib_m5gm
	$(SILENCE)install -m 440 include/lib_m5gm/* $(INCLUDE_DIR)/lib_m5gm/
	$(SILENCE)mkdir -m 740 -p $(TARGET_DIR)/usr/bin/
	$(SILENCE)install -m 550 $(BUILD_PATH)/util_m5gm $(TARGET_DIR)/usr/bin/util_m5gm

clean:
	$(SILENCE)rm -rf $(BUILD_PATH)

test:
ifneq ($(shell dpkg -l cpputest > /dev/null 2>&1; echo $$?),0)
	echo "make cpputest"
	make -f cpputest.makefile
	echo "make unittest"
	make -f unittest.makefile clean gcov
	#echo "make finsih"
else
	mkdir -p $(BUILD_PATH)/unittest
	cd $(BUILD_PATH)/unittest && gcc -c $(CFLAGS) $(CPPUTEST_CFLAGS) \
		$$(find ${PWD}/src ${PWD}/test -type f -name *.c)
	cd $(BUILD_PATH)/unittest && g++ -c $(CFLAGS) $(CPPUTEST_CXXFLAGS) \
		$$(find ${PWD}/src ${PWD}/test -type f -name *.cpp)
	g++ $(CFLAGS) \
	    $$(objdump -t $(UNITTEST_OBJS) \
		| grep ".text.*__wrap_" \
		| sed -e "s/.*.text.*__wrap_\(.*\)/-Wl,--wrap=\1/") \
	    -g \
	    -o $(BUILD_PATH)/unittest/unittest \
	    $(UNITTEST_OBJS) \
	    -lCppUTest -lCppUTestExt -lgcov
# Run unittest
	mkdir -p $(RESULT_ROOT)
	rm -f $(GCOV_OUTPUT_FILE) $(GCOV_ERROR_FILE) $(TEST_REPORT_FILE) $(TEST_OUTPUT_FILE)
	$(BUILD_PATH)/unittest/unittest -v >> $(TEST_OUTPUT_FILE)
# Calculate Coverage
	for object_file in $(UNITTEST_SRC_OBJS); do \
		gcov $${object_file} >>$(GCOV_OUTPUT_FILE) 2>>$(GCOV_ERROR_FILE); \
	done
# Post Process
	/usr/share/cpputest/scripts/filterGcov.sh $(GCOV_OUTPUT_FILE) $(GCOV_ERROR_FILE) $(TEST_REPORT_FILE) $(TEST_OUTPUT_FILE)
# Output Result
	echo "###############"
	echo "# Test Detail #"
	echo "###############"
	cat $(TEST_OUTPUT_FILE)
	echo "#################"
	echo "# Test Coverage #"
	echo "#################"
	cat $(TEST_REPORT_FILE)
	awk 'NF == 2 { \
			total_coverage += $$1; \
			test_group_count++; \
		} \
		END { \
			printf "Run %d test groups with average coverage %.2f%%\n", \
				test_group_count, \
				total_coverage/test_group_count; \
		}' \
		$(TEST_REPORT_FILE)
	echo "###############"
	echo "# Test Result #"
	echo "###############"
	awk -F'[ ()]' \
		'BEGIN { \
			failures = 0; \
			tests = 0; \
			ran = 0; \
			checks = 0; \
			ignored = 0; \
			filtered_out = 0; \
			runtime = 0; \
		} \
		/Errors (.*)/ { \
			failures += $$3; \
			tests += $$5; \
			ran += $$7; \
			checks += $$9; \
			ignored += $$11; \
			filtered_out += $$13; \
			runtime += $$16; \
		} \
		/OK (.*)/{ \
			tests += $$3; \
			ran += $$5; \
			checks += $$7; \
			ignored += $$9; \
			filtered_out += $$11; \
			runtime += $$14; \
		} \
		END { \
			if (failures) { \
				result = "Errors"; \
			} else { \
				result = "OK"; \
			} \
			printf "%s (%d failures, %d tests, %d ran, %d checks, %d ignored, %d filtered out, %d ms)\n", \
				result, failures, tests, ran, checks, ignored, filtered_out, runtime; \
		}' \
		$(TEST_OUTPUT_FILE)
# Raise fail when there is test error
	! grep -q "Errors (.*)" $(TEST_OUTPUT_FILE)
# Clean Up
	rm -rf $(BUILD_PATH)
endif
