#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport_c.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

extern "C"
{
#include "lib_m5gm/lib_m5gm.h"
int32_t _m5gm_set_operating_mode_online(void);
int32_t _m5gm_set_operating_mode_offline(void);
int32_t _m5gm_set_operating_mode_reset(void);
int32_t _m5gm_set_operating_mode_low_power(void);
int32_t _m5gm_set_operating_mode_persistent_low_power(void);
int32_t _m5gm_conn_cs_attach_status(void);
int32_t _m5gm_conn_ps_attach_status(void);
int32_t _m5gm_get_imsi(char * imsi);
int32_t _m5gm_get_iccid(char * iccid);
int32_t _m5gm_load_conn_state(const char * file_path, u_long *pdh, u_long *cid);
int32_t _m5gm_save_conn_state(const char * file_path, u_long pdh, u_long cid);
int32_t _m5gm_start_network_no_lock(const char *apn);
int32_t _m5gm_stop_network_no_lock(void);
int32_t _m5gm_get_band(const char * network_mode, struct band_config *band_config);
int32_t _m5gm_set_band(const char * network_mode, struct band_config band_config);
int32_t _m5gm_set_operating_mode_qmi(const char * operating_mode);
int32_t _m5gm_gpio_out_set_value(const char* gpio_pin, const u_int32_t value, const u_int32_t delay_msec);
int32_t _m5gm_gpio_get_value(const char* gpio_pin);
}

static int _CompareStringArray(char * const aObject1[], char * const aObject2[])
{  
    int i = 0;
    while(aObject1[i] != 0 && aObject2[i] !=0)
    {
        if (strcmp(aObject1[i], aObject2[i]) != 0)
            return 0;
        i++;
    }
    if (aObject1[i] == 0 && aObject2[i] ==0)
    	return 1;
    else
       return 0;
}

static const char *_StringArrayValueToString(const void *aObject)
{
    size_t buffer_len = 2048 -1;
    static char command_buffer[2048] = {0};
    int32_t index = 0;
    char ** argv = (char **) aObject;
    memset(command_buffer, 0, sizeof(command_buffer));
    
    if(!argv)
        return NULL;
    
    while(argv[index])
    {
        strncat(command_buffer, argv[index], buffer_len - 1);
        buffer_len = buffer_len - strlen(argv[index]);
        strncat(command_buffer, " ", buffer_len - 1);
        buffer_len--;
        index++;
    }
    return command_buffer;
}

TEST_GROUP(lib_m5gm)
{
    void setup(){
            mock_c()->installComparator("StringArray", (int (*)(const void*, const void*))_CompareStringArray, _StringArrayValueToString);
            mkdir("./test", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        };
    void teardown(){ 
            mock_c()->clear();
            mock_c()->removeAllComparatorsAndCopiers();
            remove(CONN_STATE_FILE_PATH);
            remove("test/output");
            remove("test/output2");
        };
};

TEST(lib_m5gm, Test_m5gm_init)
{
    // this case will change global variable
    // shall be executed as last one.
    struct m5gm_path_config config;
    snprintf(config.WWAN_DEV, MAX_PATH_LEN, "1");
    snprintf(config.QMICLI, MAX_PATH_LEN, "2");
    snprintf(config.QMI_NETWORK_PATH, MAX_PATH_LEN, "3");
    snprintf(config.M5GM_CONTROL_PATH, MAX_PATH_LEN, "4");
    snprintf(config.CDC_WDM, MAX_PATH_LEN, "5");
    snprintf(config.DHCPCD, MAX_PATH_LEN, "6");
    snprintf(config.IFCONFIG, MAX_PATH_LEN, "7");
    snprintf(config.AT_DEV, MAX_PATH_LEN, "8");
    snprintf(config.MXAT_PATH, MAX_PATH_LEN, "9");
    int32_t result = m5gm_init(config);
    CHECK_EQUAL(0, result);
    STRCMP_EQUAL(WWAN_DEV, "1");
    STRCMP_EQUAL(QMICLI, "2");
    STRCMP_EQUAL(QMI_NETWORK_PATH, "3");
    STRCMP_EQUAL(M5GM_CONTROL_PATH, "4");
    STRCMP_EQUAL(CDC_WDM, "5");
    STRCMP_EQUAL(DHCPCD, "6");
    STRCMP_EQUAL(IFCONFIG, "7");
    STRCMP_EQUAL(AT_DEV, "8");
    STRCMP_EQUAL(MXAT_PATH, "9");
}

TEST(lib_m5gm, Test__m5gm_load_conn_state)
{
    remove(CONN_STATE_FILE_PATH);
    FILE * output = fopen(CONN_STATE_FILE_PATH,"w+");
    fprintf(output, "PDH=3923577762\n");
    fprintf(output, "CID=26\n");
    fseek(output, 0, SEEK_SET);
    fclose(output);
    u_long pdh;
    u_long cid;
    int32_t result = _m5gm_load_conn_state(CONN_STATE_FILE_PATH, &pdh, &cid);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_load_conn_state_fail)
{
    u_long pdh;
    u_long cid;
    int32_t result = _m5gm_load_conn_state("/notexistedpath/notexistedfile", &pdh, &cid);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_load_conn_state_fail2)
{
    remove(CONN_STATE_FILE_PATH);
    FILE * output = fopen(CONN_STATE_FILE_PATH,"w+");
    fprintf(output, "CID=26\n");
    fseek(output, 0, SEEK_SET);
    fclose(output);
    u_long pdh;
    u_long cid;
    int32_t result = _m5gm_load_conn_state(CONN_STATE_FILE_PATH, &pdh, &cid);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test__m5gm_load_conn_state_fail3)
{
    remove(CONN_STATE_FILE_PATH);
    FILE * output = fopen(CONN_STATE_FILE_PATH,"w+");
    fprintf(output, "PDH=3923577762\n");
    fseek(output, 0, SEEK_SET);
    fclose(output);
    u_long pdh;
    u_long cid;
    int32_t result = _m5gm_load_conn_state(CONN_STATE_FILE_PATH, &pdh, &cid);
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test__m5gm_save_conn_state)
{
    u_long pdh = 0;
    u_long cid = 0;
    int32_t result = _m5gm_save_conn_state(CONN_STATE_FILE_PATH, 3923577763, 23);
    FILE * state_file = fopen(CONN_STATE_FILE_PATH,"r");
    fscanf(state_file, "PDH=%lu\n", &pdh);
    fscanf(state_file, "CID=%lu\n", &cid);
    fseek(state_file, 0, SEEK_SET);
    fclose(state_file);
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(3923577763, pdh);
    CHECK_EQUAL(23, cid);
}

TEST(lib_m5gm, Test__m5gm_save_conn_state_fail)
{
    int32_t result = _m5gm_save_conn_state("/notexistedpath/notexistedfile", 3923577762, 26);
    CHECK_EQUAL(-1, result);
}


TEST(lib_m5gm, Test__m5gm_start_network_no_lock)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--wds-start-network=apn=internet,ip-type=4,3gpp-profile=1", "--client-no-release-cid", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "Packet data handle: '3923577762'\n");
    fprintf(output, "CID: '26'\n");
    fprintf(output, "[/dev/cdc-wdm0] Network started\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_start_network_no_lock("internet");
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_start_network_no_lock_fail)
{
    int32_t result = _m5gm_start_network_no_lock(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_stop_network_no_lock)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--wds-stop-network=3923577760", "--client-cid=28", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "[/dev/cdc-wdm0] Network stopped\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    _m5gm_save_conn_state(CONN_STATE_FILE_PATH, 3923577760,28);
    int32_t result = _m5gm_stop_network_no_lock();
    CHECK_EQUAL(0, result);
    
}

TEST(lib_m5gm, Test__m5gm_stop_network_no_lock_fail)
{
    remove(CONN_STATE_FILE_PATH);
    int32_t result = _m5gm_stop_network_no_lock();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_conn_start)
{
    int32_t result = m5gm_conn_start(NULL, 0);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_conn_start_with_apn)
{
    u_long pdh;
    u_long cid;
    char command_buffer[512][256] = {0};
    int i = 0;
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    for(i = 15; i < 256; i++)
    {
        sprintf(command_buffer[i], "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%d", i);
        mock_c()->expectOneCall("system")->withStringParameters("command", command_buffer[i])->andReturnIntValue(0);
    }

    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo Y > /sys/class/net/cell_wan/qmi/raw_ip")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan up")->andReturnIntValue(0);
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--wds-start-network=apn=internet,ip-type=4,3gpp-profile=1", "--client-no-release-cid", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "Packet data handle: '3923577762'\n");
    fprintf(output, "CID: '26'\n");
    fprintf(output, "[/dev/cdc-wdm0] Network started\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/udhcpc -n -i cell_wan -p /var/run/udhcpc-cell_wan.pid")->andReturnIntValue(0);
    int32_t result = m5gm_conn_start("internet", 0);
    _m5gm_load_conn_state(CONN_STATE_FILE_PATH, &pdh, &cid);
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(3923577762, pdh);
    CHECK_EQUAL(26, cid);
}

TEST(lib_m5gm, Test_m5gm_conn_start_fail_1)
{
    char command_buffer[512][256] = {0};
    int i = 0;
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    for(i = 15; i < 256; i++)
    {
        sprintf(command_buffer[i], "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%d", i);
        mock_c()->expectOneCall("system")->withStringParameters("command", command_buffer[i])->andReturnIntValue(0);
    }

    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo Y > /sys/class/net/cell_wan/qmi/raw_ip")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan up")->andReturnIntValue(-1);
    int32_t result = m5gm_conn_start("internet", 0);
    CHECK_EQUAL(-4, result);
}

TEST(lib_m5gm, Test_m5gm_conn_start_fail_2)
{
    char command_buffer[512][256] = {0};
    int i = 0;
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    for(i = 15; i < 256; i++)
    {
        sprintf(command_buffer[i], "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%d", i);
        mock_c()->expectOneCall("system")->withStringParameters("command", command_buffer[i])->andReturnIntValue(0);
    }

    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo Y > /sys/class/net/cell_wan/qmi/raw_ip")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan up")->andReturnIntValue(0);
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--wds-start-network=apn=internet,ip-type=4,3gpp-profile=1", "--client-no-release-cid", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "Packet data handle: '3923577763'\n");
    fprintf(output, "CID: '27'\n");
    fprintf(output, "[/dev/cdc-wdm0] Network not really started\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("system")->withStringParameters("command", "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=27")->andReturnIntValue(0);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_conn_start("internet", 0);
    CHECK_EQUAL(-5, result);
}

TEST(lib_m5gm, Test_m5gm_conn_start_fail_3)
{
    u_long pdh;
    u_long cid;
    char command_buffer[512][256] = {0};
    int i = 0;
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    for(i = 15; i < 256; i++)
    {
        sprintf(command_buffer[i], "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%d", i);
        mock_c()->expectOneCall("system")->withStringParameters("command", command_buffer[i])->andReturnIntValue(0);
    }


    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo Y > /sys/class/net/cell_wan/qmi/raw_ip")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan up")->andReturnIntValue(0);
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--wds-start-network=apn=internet,ip-type=4,3gpp-profile=1", "--client-no-release-cid", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "Packet data handle: '3923577763'\n");
    fprintf(output, "CID: '27'\n");
    fprintf(output, "[/dev/cdc-wdm0] Network started\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/udhcpc -n -i cell_wan -p /var/run/udhcpc-cell_wan.pid")->andReturnIntValue(-1);
    int32_t result = m5gm_conn_start("internet", 0);
    _m5gm_load_conn_state(CONN_STATE_FILE_PATH, &pdh, &cid);
    CHECK_EQUAL(-6, result);
    CHECK_EQUAL(3923577763, pdh);
    CHECK_EQUAL(27, cid);
}

TEST(lib_m5gm, Test_m5gm_conn_start_fail_4)
{
    char command_buffer[512][256] = {0};
    int i = 0;
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    for(i = 15; i < 256; i++)
    {
        sprintf(command_buffer[i], "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%d", i);
        mock_c()->expectOneCall("system")->withStringParameters("command", command_buffer[i])->andReturnIntValue(0);
    }

    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(-1);
    int32_t result = m5gm_conn_start("internet", 0);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_conn_start_fail_5)
{
    char command_buffer[512][256] = {0};
    int i = 0;
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    for(i = 15; i < 256; i++)
    {
        sprintf(command_buffer[i], "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%d", i);
        mock_c()->expectOneCall("system")->withStringParameters("command", command_buffer[i])->andReturnIntValue(0);
    }

    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo Y > /sys/class/net/cell_wan/qmi/raw_ip")->andReturnIntValue(-1);
    int32_t result = m5gm_conn_start("internet", 0);
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_conn_stop)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--wds-stop-network=3923577760", "--client-cid=28", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "[/dev/cdc-wdm0] Network stopped\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    _m5gm_save_conn_state(CONN_STATE_FILE_PATH, 3923577760,28);
    int32_t result = m5gm_conn_stop(0);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_conn_stop_fail_1)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--wds-stop-network=3923577761", "--client-cid=28", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "[/dev/cdc-wdm0] Network not really stopped\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    _m5gm_save_conn_state(CONN_STATE_FILE_PATH, 3923577761,28);
    int32_t result = m5gm_conn_stop(0);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_conn_stop_fail_2)
{
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(-1);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(-1);
    _m5gm_save_conn_state(CONN_STATE_FILE_PATH, 3923577762,28);
    int32_t result = m5gm_conn_stop(0);
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_conn_stop_fail_3)
{
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(-1);
    _m5gm_save_conn_state(CONN_STATE_FILE_PATH, 3923577763,28);
    int32_t result = m5gm_conn_stop(0);
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_conn_lte_tracking_area_code)
{
    
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--nas-get-serving-system", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "LTE tracking area code: '11200'\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_conn_lte_tracking_area_code();
    CHECK_EQUAL(11200, result);
}

TEST(lib_m5gm, Test_m5gm_conn_lte_tracking_area_code_fail)
{
    
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--nas-get-serving-system", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_conn_lte_tracking_area_code();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_conn_get_status_connected)
{
    
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--wds-get-packet-service-status", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "status: 'connected'\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_conn_get_status();
    CHECK_EQUAL(1, result);
}

TEST(lib_m5gm, Test_m5gm_conn_get_status_disconnect)
{
    
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--wds-get-packet-service-status", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "status: 'disconnected'\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_conn_get_status();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_conn_get_status_fail)
{
    
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--wds-get-packet-service-status", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_conn_get_status();
    CHECK_EQUAL(-1, result);
}


TEST(lib_m5gm, Test__m5gm_conn_cs_attach_status_attached)
{
    
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--nas-get-serving-system", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "CS: 'attached'\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_conn_cs_attach_status();
    CHECK_EQUAL(1, result);
}

TEST(lib_m5gm, Test__m5gm_conn_cs_attach_status_detached)
{
    
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--nas-get-serving-system", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "CS: 'detached'\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_conn_cs_attach_status();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_conn_cs_attach_status_fail)
{
    
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "--nas-get-serving-system", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_conn_cs_attach_status();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_conn_ps_attach_status_attached)
{
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CGATT?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CGATT: 1\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_conn_ps_attach_status();
    CHECK_EQUAL(1, result);
}

TEST(lib_m5gm, Test__m5gm_conn_ps_attach_status_detached)
{
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CGATT?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CGATT: 0\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_conn_ps_attach_status();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_conn_ps_attach_status_fail)
{
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CGATT?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_conn_ps_attach_status();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_op_at)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QSPN", "-t", "5000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QSPN: \"Chunghwa Telecom\",\"Chunghwa\",\"\",0,\"46692\"\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_op_at("AT+QSPN", 5000);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_op_at_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QSPN", "-t", "5000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QSPN: \"Chunghwa Telecom\",\"Chunghwa\",\"\",0,\"46692\"\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_op_at("AT+QSPN", 5000);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_op_at_fail2)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QSPN", "-t", "5000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QSPN: \"OK Telecom\",\"OK\",\"\",0,\"46692\"\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_op_at("AT+QSPN", 5000);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_carrier_info)
{
    struct carrier_info info;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QSPN", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QSPN: \"Chunghwa Telecom\",\"Chunghwa\",\"\",0,\"46692\"\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_carrier_info(&info);
    CHECK_EQUAL(0, result);
    STRCMP_EQUAL("Chunghwa Telecom", info.service_provider_full_name);
    STRCMP_EQUAL("Chunghwa", info.service_provider_short_name);
    STRCMP_EQUAL("46692", info.registered_plmn);
}

TEST(lib_m5gm, Test_m5gm_get_carrier_info_fail)
{
    struct carrier_info info;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QSPN", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QSPN: \"Chunghwa Telecom\",\"Chunghwa\",\"\",0,\"46692\"\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_carrier_info(&info);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_carrier_info_fail2)
{
    struct carrier_info info;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QSPN", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_carrier_info(&info);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_carrier_info_fail3)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+COPS?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QSPN: \"Chunghwa Telecom\",\"Chunghwa\",\"\",0,\"46692\"\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_carrier_info(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_carrier_info_fail4)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+COPS?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QSPN: \"Chunghwa Telecom\",\"\",\"\",0,\"46692\"\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_carrier_info(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_signal_status_lte)
{
    struct signal_status stat;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "at+qeng=\"servingcell\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QENG: \"servingcell\",\"NOCONN\",\"LTE\",\"FDD\",466,97,3867985,17,1275,3,4,4,2CC4,-83,-14,-50,13,255,170,-\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_signal_status(&stat);
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(-50, stat.lte_rssi);
    CHECK_EQUAL(-14, stat.lte_rsrq);
    CHECK_EQUAL(-83, stat.lte_rsrp);
    CHECK_EQUAL(13.0, stat.lte_snr);
    CHECK_EQUAL(1, stat.is_lte);
}

TEST(lib_m5gm, Test_m5gm_get_signal_status_5g)
{
    struct signal_status stat;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "at+qeng=\"servingcell\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QENG:\"NR5G-NSA\",466,97,65535,-84,8,-12\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_signal_status(&stat);
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(1, stat.is_5g);
    CHECK_EQUAL(-84, stat.rsrp_5g);
    CHECK_EQUAL(-12, stat.rsrq_5g);
    CHECK_EQUAL(8.0, stat.snr_5g);
}

TEST(lib_m5gm, Test_m5gm_get_signal_status_wcdma)
{
    struct signal_status stat;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "at+qeng=\"servingcell\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QENG: \"servingcell\",\"NOCONN\",\"WCDMA\",466,97,2B6A,921037,10738,328,59,-74,-3,-,-,-,-,-\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_signal_status(&stat);
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(1, stat.is_wcdma);
    CHECK_EQUAL(-74, stat.wcdma_rssi);
    CHECK_EQUAL(-3, stat.wcdma_ecio);
}

TEST(lib_m5gm, Test_m5gm_get_signal_status_fail)
{
    struct signal_status stat;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "at+qeng=\"servingcell\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR:\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_signal_status(&stat);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_signal_status_fail_2)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "at+qeng=\"servingcell\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR:\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_signal_status(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_info)
{
    struct module_info info;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "ATI", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ATI\n");
    fprintf(output, "Quectel\n");
    fprintf(output, "RM500QGL\n");
    fprintf(output, "Revision: RM500QGLABR01A01M4G\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_get_info(&info);
    CHECK_EQUAL(0, result);
    STRCMP_EQUAL("Quectel", info.manufacturer);
    STRCMP_EQUAL("RM500QGL", info.module_name);
    STRCMP_EQUAL("RM500QGLABR01A01M4G", info.firmware_version);
}

TEST(lib_m5gm, Test_m5gm_module_get_info_fail)
{
    struct module_info info;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "ATI", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ATI\n");
    fprintf(output, "ERROR\n");
    fprintf(output, "Revision: RM500QGLABR01A01M4G\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_get_info(&info);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_info_fail_2)
{
    int32_t result = m5gm_module_get_info(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_info_fail_3)
{
    struct module_info info;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "ATI", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ATI\n");
    fprintf(output, "Quectel\n");
    fprintf(output, "RM500QGL\n");
    fprintf(output, "Revision: RM500QGLABR01A01M4G\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_get_info(&info);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_operational_mode_online)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CFUN: 1");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_operational_mode();
    CHECK_EQUAL(OP_MODE_ONLINE, result);
}

TEST(lib_m5gm, Test_m5gm_get_operational_mode_offline)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CFUN: 7");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_operational_mode();
    CHECK_EQUAL(OP_MODE_OFFLINE, result);
}
#if 0
TEST(lib_m5gm, Test_m5gm_get_operational_mode_reset)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "-p", "--dms-get-operating-mode", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "Mode: 'reset'\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_operational_mode();
    CHECK_EQUAL(OP_MODE_RESET, result);
}
#endif
TEST(lib_m5gm, Test_m5gm_get_operational_mode_low_power_0)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CFUN: 0");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_operational_mode();
    CHECK_EQUAL(OP_MODE_LOW_POWER, result);
}

TEST(lib_m5gm, Test_m5gm_get_operational_mode_low_power_4)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CFUN: 4");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_operational_mode();
    CHECK_EQUAL(OP_MODE_LOW_POWER, result);
}
#if 0
TEST(lib_m5gm, Test_m5gm_get_operational_mode_persistent_low_power)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CFUN: 4");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_operational_mode();
    CHECK_EQUAL(OP_MODE_PERSISTENT_LOW_POWER, result);
}
#endif

TEST(lib_m5gm, Test_m5gm_get_operational_mode_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_operational_mode();
    CHECK_EQUAL(OP_MODE_ERROR, result);
}

TEST(lib_m5gm, Test_m5gm_get_imei)
{
    char imei[1024] = {0};
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "-p", "--dms-get-ids", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "        IMEI: '863305040086315'\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_imei(imei);
    CHECK_EQUAL(0, result);
    STRCMP_EQUAL("863305040086315", imei);
}

TEST(lib_m5gm, Test_m5gm_get_imei_fail)
{
    char imei[1024] = {0};
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "-p", "--dms-get-ids", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "        ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_imei(imei);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_imei_fail_2)
{
    int32_t result = m5gm_get_imei(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_set_operating_mode_online)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=1,0", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, AT_COMMAND_SUCCESS);
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_set_operating_mode_online();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_set_operating_mode_online_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=1,0", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_set_operating_mode_online();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_set_operating_mode_offline)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "-p", "--dms-set-operating-mode=offline", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "Operating mode set successfully\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_set_operating_mode_offline();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_set_operating_mode_offline_fail)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "-p", "--dms-set-operating-mode=offline", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_set_operating_mode_offline();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_set_operating_mode_reset)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "-p", "--dms-set-operating-mode=reset", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "Operating mode set successfully\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_set_operating_mode_reset();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_set_operating_mode_reset_fail)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "-p", "--dms-set-operating-mode=reset", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_set_operating_mode_reset();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_set_operating_mode_low_power)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=4,0", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, AT_COMMAND_SUCCESS);
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_set_operating_mode_low_power();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_set_operating_mode_low_power_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=4,0", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_set_operating_mode_low_power();
    CHECK_EQUAL(-1, result);
}


TEST(lib_m5gm, Test__m5gm_set_operating_mode_persistent_low_power)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "-p", "--dms-set-operating-mode=persistent-low-power", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "Operating mode set successfully\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_set_operating_mode_persistent_low_power();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_set_operating_mode_persistent_low_power_fail)
{
    const char * const argv[] = {"/usr/bin/qmicli", "-d", "/dev/cdc-wdm0", "-p", "--dms-set-operating-mode=persistent-low-power", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_set_operating_mode_persistent_low_power();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_set_flight_mode_enable)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=4,0", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    char command_buffer[512][256] = {0};
    int i = 0;
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    for(i = 15; i < 256; i++)
    {
        sprintf(command_buffer[i], "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%d", i);
        mock_c()->expectOneCall("system")->withStringParameters("command", command_buffer[i])->andReturnIntValue(0);
    }


    fprintf(output, AT_COMMAND_SUCCESS);
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_flight_mode(1);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_flight_mode_enable_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=4,0", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");

    char command_buffer[512][256] = {0};
    int i = 0;
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    for(i = 15; i < 256; i++)
    {
        sprintf(command_buffer[i], "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%d", i);
        mock_c()->expectOneCall("system")->withStringParameters("command", command_buffer[i])->andReturnIntValue(0);
    }


    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_flight_mode(1);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_set_flight_mode_disable)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=1,0", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, AT_COMMAND_SUCCESS);
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_flight_mode(0);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_flight_mode_disable_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=1,0", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_flight_mode(0);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_get_imsi)
{
    char imsi[1024] = {0};
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CIMI", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CIMI\n");
    fprintf(output, "466924203580833\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_get_imsi(imsi);
    CHECK_EQUAL(0, result);
    STRCMP_EQUAL("466924203580833", imsi);
}

TEST(lib_m5gm, Test__m5gm_get_imsi_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CIMI", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CIMI\n");
    fprintf(output, "466924203580833\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_get_imsi(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_get_imsi_fail2)
{
    char imsi[1024] = {0};
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CIMI", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CIMI\n");
    fprintf(output, "466924203580833\n");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_get_imsi(imsi);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_get_iccid)
{
    char iccid[1024] = {0};
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+ICCID", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+ICCID\n");
    fprintf(output, "+ICCID: 89886920042035808331\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_get_iccid(iccid);
    CHECK_EQUAL(0, result);
    STRCMP_EQUAL("89886920042035808331", iccid);
}

TEST(lib_m5gm, Test__m5gm_get_iccid_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+ICCID", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+ICCID\n");
    fprintf(output, "+ICCID: 89886920042035808331\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_get_iccid(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_get_iccid_fail2)
{
    char iccid[1024] = {0};
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+ICCID", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+ICCID\n");
    fprintf(output, "+ICCID: 89886920042035808331\n");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = _m5gm_get_iccid(iccid);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_temp)
{
    struct module_temperature temperature = {255,255,255,255,255,255,255,255,255,255};
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QTEMP", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+QTEMP\n");
    fprintf(output, "+QTEMP:\"qfe_wtr_pa0\",\"31\"\n");
    fprintf(output, "+QTEMP:\"aoss0-usr\",\"32\"\n");
    fprintf(output, "+QTEMP:\"mdm-q6-usr\",\"33\"\n");
    fprintf(output, "+QTEMP:\"ipa-usr\",\"34\"\n");
    fprintf(output, "+QTEMP:\"cpu0-a7-usr\",\"35\"\n");
    fprintf(output, "+QTEMP:\"mdm-5g-usr\",\"36\"\n");
    fprintf(output, "+QTEMP:\"mdm-vpe-usr\",\"37\"\n");
    fprintf(output, "+QTEMP:\"mdm-core-usr\",\"38\"\n");
    fprintf(output, "+QTEMP:\"xo-therm-usr\",\"39\"\n");
    fprintf(output, "+QTEMP:\"sdx-case-therm-usr\",\"40\"\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_get_temp(&temperature);
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(31, temperature.qfe_wtr_pa0);
    CHECK_EQUAL(32, temperature.aoss0_usr);
    CHECK_EQUAL(33, temperature.mdm_q6_usr);
    CHECK_EQUAL(34, temperature.ipa_usr);
    CHECK_EQUAL(35, temperature.cpu0_a7_usr);
    CHECK_EQUAL(36, temperature.mdm_5g_usr);
    CHECK_EQUAL(37, temperature.mdm_vpe_usr);
    CHECK_EQUAL(38, temperature.mdm_core_usr);
    CHECK_EQUAL(39, temperature.xo_therm_usr);
    CHECK_EQUAL(40, temperature.sdx_case_therm_usr);

}

TEST(lib_m5gm, Test_m5gm_module_get_temp_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QTEMP", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+QTEMP\n");
    fprintf(output, "+ICCID: 89886920042035808331\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_get_temp(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_temp_fail2)
{
    struct module_temperature temperature = {255,255,255,255,255,255,255,255,255,255};
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QTEMP", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+QTEMP\n");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_get_temp(&temperature);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_pin_protection_disable)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",2", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CLCK=\"SC\",2\n");
    fprintf(output, "+CLCK: 0\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_sim_get_pin_protection();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_pin_protection_enable)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",2", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CLCK=\"SC\",2\n");
    fprintf(output, "+CLCK: 1\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_sim_get_pin_protection();
    CHECK_EQUAL(1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_pin_protection_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",2", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CLCK=\"SC\",2\n");
    fprintf(output, "+CLCK: 0\n");
    fprintf(output, "ERROR\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_sim_get_pin_protection();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_pin_protection_fail2)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",2", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CLCK=\"SC\",2\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_sim_get_pin_protection();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_pin_protection_fail3)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",2", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CLCK=\"SC\",2\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_sim_get_pin_protection();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_info)
{
    struct sim_info info;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CIMI", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CIMI\n");
    fprintf(output, "466924203580833\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    
    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+ICCID", "-t", "10000", NULL};
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "AT+ICCID\n");
    fprintf(output2, "+ICCID: 89886920042035808331\n");
    fprintf(output2, "OK\r\n");
    fseek(output2, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output2);

    int32_t result = m5gm_sim_get_info(&info);
    CHECK_EQUAL(0, result);
    STRCMP_EQUAL("466924203580833", info.imsi);
    STRCMP_EQUAL("89886920042035808331", info.iccid);
}

TEST(lib_m5gm, Test_m5gm_sim_get_info_fail1)
{
    struct sim_info info;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CIMI", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CIMI\n");
    //fprintf(output, "466924203580833\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    
    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+ICCID", "-t", "10000", NULL};
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "AT+ICCID\n");
    fprintf(output2, "+ICCID: 89886920042035808331\n");
    fprintf(output2, "OK\r\n");
    fseek(output2, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output2);

    int32_t result = m5gm_sim_get_info(&info);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_info_fail2)
{
    struct sim_info info;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CIMI", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CIMI\n");
    fprintf(output, "466924203580833\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    
    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+ICCID", "-t", "10000", NULL};
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "AT+ICCID\n");
    //fprintf(output2, "+ICCID: 89886920042035808331\n");
    fprintf(output2, "OK\r\n");
    fseek(output2, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output2);

    int32_t result = m5gm_sim_get_info(&info);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_info_fail3)
{
    int32_t result = m5gm_sim_get_info(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_attach_status_attached)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CGATT?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CGATT: 1\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_attach_status();
    CHECK_EQUAL(1, result);
}

TEST(lib_m5gm, Test_m5gm_get_attach_status_detached)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CGATT?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CGATT: 0\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_attach_status();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_pin_retry)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QPINC=\"SC\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QPINC: \"SC\",3,10\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_sim_get_pin_retry();
    CHECK_EQUAL(3, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_pin_retry_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QPINC=\"SC\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_sim_get_pin_retry();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_get_pin_retry_fail2)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QPINC=\"SC\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QPINC: \"SC\",3,10\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_sim_get_pin_retry();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_code)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",2", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CLCK=\"SC\",2\n");
    fprintf(output, "+CLCK: 1\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);

    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPWD=\"SC\",\"0000\",\"1111\"", "-t", "10000", NULL};
    FILE * output2 = fopen("./test/output","w+");
    fprintf(output2, "OK\r\n");
    fseek(output2, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output2);
    int32_t result = m5gm_sim_set_pin_code("0000", "1111");
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_code_fail)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",2", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CLCK=\"SC\",2\n");
    fprintf(output, "+CLCK: 0\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_set_pin_code("0000", "1111");
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_code_fail2)
{
    int32_t result = m5gm_sim_set_pin_code("100000001", "1111");
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_code_fail3)
{
    int32_t result = m5gm_sim_set_pin_code("0000", "111111111");
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_code_fail4)
{
    int32_t result = m5gm_sim_set_pin_code("0000", "-1");
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_code_fail5)
{
    int32_t result = m5gm_sim_set_pin_code("-10000000", "1111");
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_code_fail6)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",2", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "AT+CLCK=\"SC\",2\n");
    fprintf(output, "+CLCK: 1\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);

    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPWD=\"SC\",\"0000\",\"1111\"", "-t", "10000", NULL};
    FILE * output2 = fopen("./test/output","w+");
    fseek(output2, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output2);
    int32_t result = m5gm_sim_set_pin_code("0000", "1111");
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_unlock_pin)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPIN=\"0000\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_unlock_pin("0000");
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_sim_unlock_pin_fail)
{
    int32_t result = m5gm_sim_unlock_pin("100000001");
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_sim_unlock_pin_fail2)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPIN=\"0000\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_unlock_pin("0000");
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_unlock_pin_fail3)
{
    int32_t result = m5gm_sim_unlock_pin("-1");
    CHECK_EQUAL(-2, result);
}


TEST(lib_m5gm, Test_m5gm_sim_pin_status_ready)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPIN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CPIN: READY\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_pin_status();
    CHECK_EQUAL(1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_pin_status_sim_pin)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPIN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CPIN: SIM PIN\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_pin_status();
    CHECK_EQUAL(PIN_PASSWORD_MODE_SIM_PIN, result);
}

TEST(lib_m5gm, Test_m5gm_sim_pin_status_sim_puk)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPIN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CPIN: SIM PUK\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_pin_status();
    CHECK_EQUAL(PIN_PASSWORD_MODE_SIM_PUK, result);
}

TEST(lib_m5gm, Test_m5gm_sim_pin_status_sim_pin2)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPIN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CPIN: SIM PIN2\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_pin_status();
    CHECK_EQUAL(PIN_PASSWORD_MODE_SIM_PIN2, result);
}

TEST(lib_m5gm, Test_m5gm_sim_pin_status_sim_puk2)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPIN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CPIN: SIM PUK2\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_pin_status();
    CHECK_EQUAL(PIN_PASSWORD_MODE_SIM_PUK2, result);
}

TEST(lib_m5gm, Test_m5gm_sim_pin_status_unknown)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPIN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CPIN: XXXXX\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_pin_status();
    CHECK_EQUAL(PIN_PASSWORD_MODE_UNKNOWN, result);
}

TEST(lib_m5gm, Test_m5gm_sim_pin_status_fail2)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CPIN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CPIN: SIM PUK2\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_pin_status();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_protection_enable)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",0,\"0000\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_set_pin_protection(0, "0000");
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_protection_disable)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",0,\"0001\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_set_pin_protection(0, "0001");
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_protection_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CLCK=\"SC\",0,\"0001\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_sim_set_pin_protection(0, "0001");
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_protection_fail2)
{
    int32_t result = m5gm_sim_set_pin_protection(2, "0001");
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_protection_fail3)
{
    int32_t result = m5gm_sim_set_pin_protection(1, "100000001");
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_sim_set_pin_protection_fail4)
{
    int32_t result = m5gm_sim_set_pin_protection(1, "-1111111");
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_event_monitor_stop)
{
    event_monitor_thread_run = 1;
    mock_c()->expectOneCall("pthread_join")\
        ->withUnsignedLongIntParameters("thread", event_monitor_thread_t)\
        ->withPointerParameters("retval", NULL)\
        ->andReturnIntValue(0);

    int32_t result = m5gm_event_monitor_stop();
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(0, event_monitor_thread_run);
}

TEST(lib_m5gm, Test_m5gm_event_monitor_start)
{
    event_monitor_thread_run = 0;
    mock_c()->expectOneCall("pthread_create")
        ->withPointerParameters("start_routine", (void*) _event_monitor_thread)\
        ->andReturnIntValue(0);

    int32_t result = m5gm_event_monitor_start();
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(1, event_monitor_thread_run);
    
}

TEST(lib_m5gm, Test_m5gm_set_ps_attach_state_attach)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CGATT=1", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_set_ps_attach_state(CARRIER_ATTACH);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_ps_attach_state_detach)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CGATT=0", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    char command_buffer[512][256] = {0};
    int i = 0;
    
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    for(i = 15; i < 256; i++)
    {
        sprintf(command_buffer[i], "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%d", i);
        mock_c()->expectOneCall("system")->withStringParameters("command", command_buffer[i])->andReturnIntValue(0);
    }
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_set_ps_attach_state(CARRIER_DETACH);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_ps_attach_state_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CGATT=0", "-t", "10000", NULL};
    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CGATT=0", "-t", "10000", NULL};
    char command_buffer[512][256] = {0};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "?\r\n");
    fseek(output, 0, SEEK_SET);
    int i = 0;
    mock_c()->expectOneCall("system")->withStringParameters("command", "kill `cat /var/run/udhcpc-cell_wan.pid`")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "/sbin/ifconfig cell_wan down")->andReturnIntValue(0);
    for(i = 15; i < 256; i++)
    {
        sprintf(command_buffer[i], "/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%d", i);
        mock_c()->expectOneCall("system")->withStringParameters("command", command_buffer[i])->andReturnIntValue(0);
    }

    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_set_ps_attach_state(CARRIER_DETACH);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_set_ps_attach_state_fail2)
{
    int32_t result = m5gm_set_ps_attach_state(2);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_manual_operator_selection_auto)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+COPS=0,2,\"46692\",7", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_manual_operator_selection(OP_SELECT_AUTO, 46692, OP_ACCESS_EUTRAN);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_manual_operator_selection_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+COPS=1,2,\"46692\",7", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "??\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);

    int32_t result = m5gm_manual_operator_selection(OP_SELECT_MANUAL, 46692, OP_ACCESS_EUTRAN);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_manual_operator_selection_fail2)
{
    int32_t result = m5gm_manual_operator_selection((u_int32_t)-1, 46692, OP_ACCESS_EUTRAN);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_manual_operator_selection_fail3)
{
    int32_t result = m5gm_manual_operator_selection(OP_SELECT_MANUAL, 46692, 13);
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_module_scan_site_lte)
{
    struct neighbour_cell_status cell_status;
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CREG?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CREG: 2,1,\"27F9\",\"E9D42A\",7\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);

    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QENG=\"neighbourcell\"", "-t", "10000", NULL};
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "+QENG: \"neighbourcell intra\",\"LTE\",38950,276,-3,-88,-65,0,37,7,16,6,44\r\n");
    fprintf(output2, "+QENG: \"neighbourcell inter\",\"LTE\",39148,-,-,-,-,-,37,0,30,7,-,-,-,-\r\n");
    fprintf(output2, "+QENG: \"neighbourcell inter\",\"LTE\",37900,-,-,-,-,-,0,0,30,6,-,-,-,-\r\n");
    fprintf(output2, "+QENG: \"neighbourcell\",\"WCDMA\",-,-,-,-,-,-200,-20,-\r\n");
    fprintf(output2, "OK\r\n");
    fseek(output2, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output2);

    int32_t result = m5gm_module_scan_site(&cell_status);
    CHECK_EQUAL(276, cell_status.cell[0].cellid);
    CHECK_EQUAL(37, cell_status.cell[1].lte_rsrq);
    CHECK_EQUAL(6, cell_status.cell[2].lte_snr);
    CHECK_EQUAL(-20, cell_status.cell[3].wcdma_ecio);
    CHECK_EQUAL(4, result);
}

TEST(lib_m5gm, Test_m5gm_module_scan_site_wcdma)
{
    struct neighbour_cell_status cell_status;
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CREG?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CREG: 2,1,\"27F9\",\"E9D42A\",2\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);

    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QENG=\"neighbourcell\"", "-t", "10000", NULL};
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "+QENG: \"neighbourcell\",\"WCDMA\",10787,-73,179,-830,-100,2,-24,-\r\n");
    fprintf(output2, "+QENG: \"neighbourcell\",\"WCDMA\",10787,-73,429,-870,-140,2,-29,-\r\n");
    fprintf(output2, "+QENG: \"neighbourcell\",\"LTE\",-,223,-200,-100,-\r\n");
    fprintf(output2, "OK\r\n");
    fseek(output2, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output2);

    int32_t result = m5gm_module_scan_site(&cell_status);
    CHECK_EQUAL(-100, cell_status.cell[0].wcdma_ecio);
    CHECK_EQUAL(-870, cell_status.cell[1].wcdma_rssi);
    CHECK_EQUAL(-200, cell_status.cell[2].lte_rsrq);
    CHECK_EQUAL(3, result);
}

TEST(lib_m5gm, Test_m5gm_module_scan_site_fail)
{
    struct neighbour_cell_status cell_status;
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CREG?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CREG: 2,1,\"27F9\",\"E9D42A\",7\r\n");
    fseek(output, 0, SEEK_SET);

    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QENG=\"neighbourcell\"", "-t", "10000", NULL};
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "+QENG: \"neighbourcell intra\",\"LTE\",38950,276,-3,-88,-65,0,37,7,16,6,44\r\n");
    fprintf(output2, "+QENG: \"neighbourcell inter\",\"LTE\",39148,-,-,-,-,-,37,0,30,7,-,-,-,-\r\n");
    fprintf(output2, "+QENG: \"neighbourcell inter\",\"LTE\",37900,-,-,-,-,-,0,0,30,6,-,-,-,-\r\n");
    fprintf(output2, "OK\r\n");
    fseek(output2, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output2);

    int32_t result = m5gm_module_scan_site(&cell_status);
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_module_scan_site_fail2)
{
    struct neighbour_cell_status cell_status;
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CREG?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CREG: 2,1,\"27F9\",\"E9D42A\",99\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);

    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QENG=\"neighbourcell\"", "-t", "10000", NULL};
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "+QENG: \"neighbourcell intra\",\"LTE\",38950,276,-3,-88,-65,0,37,7,16,6,44\r\n");
    fprintf(output2, "+QENG: \"neighbourcell inter\",\"LTE\",39148,-,-,-,-,-,37,0,30,7,-,-,-,-\r\n");
    fprintf(output2, "+QENG: \"neighbourcell inter\",\"LTE\",37900,-,-,-,-,-,0,0,30,6,-,-,-,-\r\n");
    fprintf(output2, "OK\r\n");
    fseek(output2, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output2);

    int32_t result = m5gm_module_scan_site(&cell_status);
    CHECK_EQUAL(-4, result);
}

TEST(lib_m5gm, Test_m5gm_module_scan_site_fail3)
{
    struct neighbour_cell_status cell_status;
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CREG?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CREG: 2,1,\"27F9\",\"E9D42A\",7\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);

    const char * const argv2[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QENG=\"neighbourcell\"", "-t", "10000", NULL};
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "+QENG: \"neighbourcell intra\",\"LTE\",38950,276,-3,-88,-65,0,37,7,16,6,44\r\n");
    fprintf(output2, "+QENG: \"neighbourcell inter\",\"LTE\",39148,-,-,-,-,-,37,0,30,7,-,-,-,-\r\n");
    fprintf(output2, "+QENG: \"neighbourcell inter\",\"LTE\",37900,-,-,-,-,-,0,0,30,6,-,-,-,-\r\n");
    fseek(output2, 0, SEEK_SET);
    
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv2)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output2);

    int32_t result = m5gm_module_scan_site(&cell_status);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_module_scan_site_fail4)
{
    int32_t result = m5gm_module_scan_site(NULL);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_set_network_search_mode_auto)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    mode.is_auto = 1;
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\",AUTO", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_network_search_mode(mode);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_network_search_mode_wcdma)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    mode.wcdma = 1;
    
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\",:WCDMA", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_network_search_mode(mode);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_network_search_mode_lte)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    mode.lte = 1;
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\",:LTE", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_network_search_mode(mode);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_network_search_mode_nr5g)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    mode.nr5g = 1;
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\",:NR5G", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_network_search_mode(mode);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_network_search_mode_wcdma_nr5g)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    mode.nr5g = 1;
    mode.wcdma = 1;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\",:WCDMA:NR5G", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_network_search_mode(mode);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_network_search_mode_fail)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    mode.nr5g = 1;
    mode.wcdma = 1;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\",:WCDMA:NR5G", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "??\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_network_search_mode(mode);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_search_mode_lte_nr5g)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"mode_pref\",LTE:NR5G\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_search_mode(&mode);
    CHECK_EQUAL(1, mode.nr5g);
    CHECK_EQUAL(1, mode.lte);
    CHECK_EQUAL(0, mode.wcdma);
    CHECK_EQUAL(0, mode.is_auto);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_search_mode_auto)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"mode_pref\",AUTO\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_search_mode(&mode);
    CHECK_EQUAL(0, mode.nr5g);
    CHECK_EQUAL(0, mode.lte);
    CHECK_EQUAL(0, mode.wcdma);
    CHECK_EQUAL(1, mode.is_auto);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_search_mode_wcdma)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"mode_pref\",WCDMA\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_search_mode(&mode);
    CHECK_EQUAL(0, mode.nr5g);
    CHECK_EQUAL(0, mode.lte);
    CHECK_EQUAL(1, mode.wcdma);
    CHECK_EQUAL(0, mode.is_auto);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_search_mode_lte)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"mode_pref\",LTE\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_search_mode(&mode);
    CHECK_EQUAL(0, mode.nr5g);
    CHECK_EQUAL(1, mode.lte);
    CHECK_EQUAL(0, mode.wcdma);
    CHECK_EQUAL(0, mode.is_auto);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_search_mode_nr5g)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"mode_pref\",NR5G\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_search_mode(&mode);
    CHECK_EQUAL(1, mode.nr5g);
    CHECK_EQUAL(0, mode.lte);
    CHECK_EQUAL(0, mode.wcdma);
    CHECK_EQUAL(0, mode.is_auto);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_search_mode_no_mode_found)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_search_mode(&mode);
    CHECK_EQUAL(0, mode.nr5g);
    CHECK_EQUAL(0, mode.lte);
    CHECK_EQUAL(0, mode.wcdma);
    CHECK_EQUAL(0, mode.is_auto);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_search_mode_fail)
{

    struct network_search_mode mode;
    memset(&mode, 0, sizeof(struct network_search_mode));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"mode_pref\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "??\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_search_mode(&mode);
    CHECK_EQUAL(0, mode.nr5g);
    CHECK_EQUAL(0, mode.lte);
    CHECK_EQUAL(0, mode.wcdma);
    CHECK_EQUAL(0, mode.is_auto);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_search_mode_fail2)
{
    int32_t result = m5gm_get_network_search_mode(NULL);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_registration_status)
{

    struct network_registration_status status;
    memset(&status, 0, sizeof(struct network_registration_status));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CREG?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CREG: 2,1,\"27F9\",\"E9D42A\",7\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_registration_status(&status);
    CHECK_EQUAL(2, status.unsolicited_result_code);
    CHECK_EQUAL(1, status.registration_status);
    STRCMP_EQUAL("27F9", status.lac);
    STRCMP_EQUAL("E9D42A", status.ci);
    CHECK_EQUAL(7, status.act);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_registration_status2)
{

    struct network_registration_status status;
    memset(&status, 0, sizeof(struct network_registration_status));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CREG?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CREG: 2,1,\"27F9\",\"E9D42A\",2\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_registration_status(&status);
    CHECK_EQUAL(2, status.unsolicited_result_code);
    CHECK_EQUAL(1, status.registration_status);
    STRCMP_EQUAL("27F9", status.lac);
    STRCMP_EQUAL("E9D42A", status.ci);
    CHECK_EQUAL(2, status.act);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_registration_status_fail)
{

    struct network_registration_status status;
    memset(&status, 0, sizeof(struct network_registration_status));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CREG?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_registration_status(&status);
    CHECK_EQUAL(0, status.unsolicited_result_code);
    CHECK_EQUAL(0, status.registration_status);
    STRCMP_EQUAL("", status.lac);
    STRCMP_EQUAL("", status.ci);
    CHECK_EQUAL(0, status.act);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_registration_status_fail2)
{

    struct network_registration_status status;
    memset(&status, 0, sizeof(struct network_registration_status));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CREG?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CREG: 2,1,\"27F9\",\"E9D42A\",7\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_registration_status(&status);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_registration_status_fail3)
{

    struct network_registration_status status;
    memset(&status, 0, sizeof(struct network_registration_status));
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CREG?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CR??G: 2,1,\"27F9\",\"E9D42A\",7\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_network_registration_status(&status);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_network_registration_status_fail4)
{
    int32_t result = m5gm_get_network_registration_status(NULL);
    CHECK_EQUAL(-2, result);
}


TEST(lib_m5gm, Test_m5gm_set_module_functionality_minimum_func)
{
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=0,0", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_module_functionality(0, 0);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_module_functionality_full_func_reset)
{

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=1,1", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_module_functionality(1, 1);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_module_functionality_no_tx_rx)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=4,0", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_module_functionality(4, 0);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_module_functionality_fail1)
{
    int32_t result = m5gm_set_module_functionality(3, 0);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_set_module_functionality_fail2)
{
    int32_t result = m5gm_set_module_functionality(5, 0);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_set_module_functionality_fail3)
{
    int32_t result = m5gm_set_module_functionality(1, 2);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_set_module_functionality_fail4)
{
    int32_t result = m5gm_set_module_functionality(0, 1);
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_set_module_functionality_fail5)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN=1,1", "-t", "20000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "FAIL\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_module_functionality(1, 1);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_module_functionality_minimum_func)
{
    
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CFUN: 0\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_module_functionality();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_get_module_functionality_full_func_reset)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CFUN: 1\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_module_functionality();
    CHECK_EQUAL(1, result);
}

TEST(lib_m5gm, Test_m5gm_get_module_functionality_no_tx_rx)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CFUN: 4\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_module_functionality();
    CHECK_EQUAL(4, result);
}

TEST(lib_m5gm, Test_m5gm_get_module_functionality_fail)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_module_functionality();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_get_module_functionality_fail2)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+CFUN?", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+CFUN: 4\r\n");
    fprintf(output, "ERROR?\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_module_functionality();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_module_fw_upgrade)
{
    const char * const argv[] = {"/usr/bin/QFirehose", "-f", "/tmp/XXXFIRMWARE/", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "[057.773]: upgrade progress 20%%\r\n");
    fprintf(output, "[057.773]: upgrade progress 30%%\r\n");
    fprintf(output, "[057.773]: upgrade progress 52%%\r\n");
    fprintf(output, "[057.773]: upgrade progress 81%%\r\n");
    fprintf(output, "[057.773]: upgrade progress 100%%\r\n");
    fprintf(output, "[057.783]: Upgrade module successfully.\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_fw_upgrade("/tmp/XXXFIRMWARE/");
    u_int32_t percent = m5gm_module_get_fw_upgrade_progress();
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(100, percent);
    percent = m5gm_module_get_fw_upgrade_progress();
    CHECK_EQUAL(0, percent);
}

TEST(lib_m5gm, Test_m5gm_module_fw_upgrade_fail)
{
    const char * const argv[] = {"/usr/bin/QFirehose", "-f", "/tmp/XXXFIRMWARE/", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "[057.773]: upgrade progress 20%%\r\n");
    fprintf(output, "[057.773]: upgrade progress 30%%\r\n");
    fprintf(output, "[057.773]: upgrade progress 52%%\r\n");
    fprintf(output, "[057.783]: Upgrade module fail.\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_fw_upgrade("/tmp/XXXFIRMWARE/");
    u_int32_t percent = m5gm_module_get_fw_upgrade_progress();
    CHECK_EQUAL(-1, result);
    CHECK_EQUAL(52, percent);
    percent = m5gm_module_get_fw_upgrade_progress();
    CHECK_EQUAL(52, percent);
}

TEST(lib_m5gm, Test_m5gm_module_fw_upgrade_fail2)
{
    const char * const argv[] = {"/usr/bin/QFirehose", "-f", "/tmp/XXXFIRMWARE/", NULL};
    FILE * output = fopen("./test/output","w+");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_fw_upgrade("/tmp/XXXFIRMWARE/");
    u_int32_t percent = m5gm_module_get_fw_upgrade_progress();
    CHECK_EQUAL(-1, result);
    CHECK_EQUAL(0, percent);
    percent = m5gm_module_get_fw_upgrade_progress();
    CHECK_EQUAL(0, percent);
}

TEST(lib_m5gm, Test_m5gm_module_fw_upgrade_fail3)
{
    int32_t result = m5gm_module_fw_upgrade(NULL);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_get_wcdma_band)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;

        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"gw_band\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"gw_band\",1:2:3:4:5:6:7:8:19\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_wcdma_band(&band_config);
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(1, band_config.band[1]);
    CHECK_EQUAL(0, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_wcdma_band_fail)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;

        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"gw_band\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_wcdma_band(&band_config);
    CHECK_EQUAL(-1, result);
    CHECK_EQUAL(0, band_config.band[1]);
    CHECK_EQUAL(0, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_wcdma_band_fail2)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;

        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"gw_band\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"gw_band\",1:2:3:4:5:6:7:8:19\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_wcdma_band(&band_config);
    CHECK_EQUAL(-1, result);
    CHECK_EQUAL(1, band_config.band[1]);
    CHECK_EQUAL(0, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_wcdma_band_fail3)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;
        
    int32_t result = m5gm_get_wcdma_band(NULL);
    CHECK_EQUAL(-2, result);
    CHECK_EQUAL(0, band_config.band[1]);
    CHECK_EQUAL(1, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_lte_band)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;

        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"lte_band\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"lte_band\",1:2:3:4:5:7:8:12:13:14:17:18:19:20:25:26:28:29:30:32:34:38:39:40:41:42:43:46:48:66:71:255\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_lte_band(&band_config);
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(1, band_config.band[1]);
    CHECK_EQUAL(0, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_lte_band_fail)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;
        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"lte_band\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_lte_band(&band_config);
    CHECK_EQUAL(-1, result);
    CHECK_EQUAL(0, band_config.band[1]);
    CHECK_EQUAL(0, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_lte_band_fail2)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;
        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"lte_band\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"lte_band\",1:2:3:4:5:7:8:12:13:14:17:18:19:20:25:26:28:29:30:32:34:38:39:40:41:42:43:46:48:66:71:255\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_lte_band(&band_config);
    CHECK_EQUAL(-1, result);
    CHECK_EQUAL(1, band_config.band[1]);
    CHECK_EQUAL(0, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_lte_band_fail3)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;
        
    int32_t result = m5gm_get_lte_band(NULL);
    CHECK_EQUAL(-2, result);
    CHECK_EQUAL(0, band_config.band[1]);
    CHECK_EQUAL(1, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_nr5g_band)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;

        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"nr5g_band\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"nr5g_band\",1:2:3:5:7:8:12:20:25:28:38:40:41:48:66:71:77:78:79:261\r\n");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_nr5g_band(&band_config);
    CHECK_EQUAL(0, result);
    CHECK_EQUAL(1, band_config.band[1]);
    CHECK_EQUAL(0, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_nr5g_band_fail)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;
        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"nr5g_band\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_nr5g_band(&band_config);
    CHECK_EQUAL(-1, result);
    CHECK_EQUAL(0, band_config.band[1]);
    CHECK_EQUAL(0, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_nr5g_band_fail2)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;
        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"nr5g_band\"", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "+QNWPREFCFG: \"nr5g_band\",1:2:3:5:7:8:12:20:25:28:38:40:41:48:66:71:77:78:79:261\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_get_nr5g_band(&band_config);
    CHECK_EQUAL(-1, result);
    CHECK_EQUAL(1, band_config.band[1]);
    CHECK_EQUAL(0, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_get_nr5g_band_fail3)
{
    struct band_config band_config = {0};
    band_config.band[1] = 0;
    band_config.band[9] = 1;
        
    int32_t result = m5gm_get_nr5g_band(NULL);
    CHECK_EQUAL(-2, result);
    CHECK_EQUAL(0, band_config.band[1]);
    CHECK_EQUAL(1, band_config.band[9]);
}

TEST(lib_m5gm, Test_m5gm_set_wcdma_band)
{
    struct band_config band_config = {0};
    band_config.band[1] = 1;
    band_config.band[20] = 1;
        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"gw_band\",:1:20", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_wcdma_band(band_config);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_wcdma_band_fail)
{
    struct band_config band_config = {0};
    band_config.band[1] = 1;
    band_config.band[20] = 1;
        
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"gw_band\",:1:20", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "??\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_wcdma_band(band_config);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_set_lte_band)
{
    struct band_config band_config = {0};
    band_config.band[1] = 1;
    band_config.band[22] = 1;
    band_config.band[30] = 1;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"lte_band\",:1:22:30", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_lte_band(band_config);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_lte_band_fail)
{
    struct band_config band_config = {0};
    band_config.band[1] = 1;
    band_config.band[22] = 1;
    band_config.band[30] = 1;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"lte_band\",:1:22:30", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "??\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_lte_band(band_config);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_set_nr5g_band)
{
    struct band_config band_config = {0};
    band_config.band[1] = 1;
    band_config.band[23] = 1;
    band_config.band[31] = 1;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"nr5g_band\",:1:23:31", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_nr5g_band(band_config);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_nr5g_band_fail)
{
    struct band_config band_config = {0};
    band_config.band[1] = 1;
    band_config.band[23] = 1;
    band_config.band[31] = 1;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"nr5g_band\",:1:23:31", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "??\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_nr5g_band(band_config);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_set_nsa_nr5g_band)
{
    struct band_config band_config = {0};
    band_config.band[1] = 1;
    band_config.band[23] = 1;
    band_config.band[32] = 1;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"nsa_nr5g_band\",:1:23:32", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_nsa_nr5g_band(band_config);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_set_nsa_nr5g_band_fail)
{
    struct band_config band_config = {0};
    band_config.band[1] = 1;
    band_config.band[23] = 1;
    band_config.band[32] = 1;
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QNWPREFCFG=\"nsa_nr5g_band\",:1:23:32", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "??\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_set_nsa_nr5g_band(band_config);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_set_band_fail)
{
    struct band_config band_config = {0};
    band_config.band[1] = 1;
    band_config.band[23] = 1;
    band_config.band[31] = 1;
    int32_t result = _m5gm_set_band(NULL, band_config);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test__m5gm_get_band_fail)
{
    struct band_config band_config = {0};
    int32_t result = _m5gm_get_band(NULL, &band_config);
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test__m5gm_set_operating_mode_fail)
{
    int32_t result = _m5gm_set_operating_mode_qmi(NULL);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_out_set_value)
{
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 255 > /sys/class/leds/5G_en/brightness")->andReturnIntValue(0);
    int32_t result = _m5gm_gpio_out_set_value("5G_en", 255, 100);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_out_set_value2)
{
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 0 > /sys/class/leds/5G_pwr/brightness")->andReturnIntValue(0);
    int32_t result = _m5gm_gpio_out_set_value("5G_pwr", 0, 100);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_out_set_value_fail1)
{
    int32_t result = _m5gm_gpio_out_set_value("5G_pwr", 256, 100);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_out_set_value_fail2)
{
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 0 > /sys/class/leds/5G_pwr/brightness")->andReturnIntValue(-1);
    int32_t result = _m5gm_gpio_out_set_value("5G_pwr", 0, 100);
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_out_set_value_fail3)
{
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 255 > /sys/class/leds/5G_en/brightness")->andReturnIntValue(-1);
    int32_t result = _m5gm_gpio_out_set_value("5G_en", 255, 100);
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_out_set_value_fail4)
{
    int32_t result = _m5gm_gpio_out_set_value(NULL, 255, 100);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_on)
{
    FILE * output1 = fopen("./test/output1","w+");
    fprintf(output1, "0");
    fseek(output1, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "0");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output1);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 255 > /sys/class/leds/5G_pwr/brightness")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 255 > /sys/class/leds/5G_en/brightness")->andReturnIntValue(0);
    int32_t result = m5gm_module_set_power_on();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_on2)
{
    FILE * output1 = fopen("./test/output1","w+");
    fprintf(output1, "255");
    fseek(output1, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output1);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    int32_t result = m5gm_module_set_power_on();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_on_fail1)
{
    FILE * output1 = fopen("./test/output1","w+");
    fprintf(output1, "0");
    fseek(output1, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "0");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output1);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 255 > /sys/class/leds/5G_pwr/brightness")->andReturnIntValue(-1);
    int32_t result = m5gm_module_set_power_on();
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_on_fail2)
{
    FILE * output1 = fopen("./test/output1","w+");
    fprintf(output1, "0");
    fseek(output1, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "0");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output1);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 255 > /sys/class/leds/5G_pwr/brightness")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 255 > /sys/class/leds/5G_en/brightness")->andReturnIntValue(-1);
    int32_t result = m5gm_module_set_power_on();
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_off)
{
    FILE * output1 = fopen("./test/output1","w+");
    fprintf(output1, "255");
    fseek(output1, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output1);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QPOWD=0", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 0 > /sys/class/leds/5G_en/brightness")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 0 > /sys/class/leds/5G_pwr/brightness")->andReturnIntValue(0);
    int32_t result = m5gm_module_set_power_off();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_off2)
{
    FILE * output1 = fopen("./test/output1","w+");
    fprintf(output1, "0");
    fseek(output1, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "0");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output1);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    int32_t result = m5gm_module_set_power_off();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_off_fail1)
{
    FILE * output1 = fopen("./test/output1","w+");
    fprintf(output1, "255");
    fseek(output1, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output1);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QPOWD=0", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 0 > /sys/class/leds/5G_en/brightness")->andReturnIntValue(-1);
    int32_t result = m5gm_module_set_power_off();
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_off_fail2)
{
    FILE * output1 = fopen("./test/output1","w+");
    fprintf(output1, "255");
    fseek(output1, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output1);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QPOWD=0", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 0 > /sys/class/leds/5G_en/brightness")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 0 > /sys/class/leds/5G_pwr/brightness")->andReturnIntValue(-1);
    int32_t result = m5gm_module_set_power_off();
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_off_fail3)
{

    FILE * output1 = fopen("./test/output1","w+");
    fprintf(output1, "255");
    fseek(output1, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output1);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QPOWD=0", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);    int32_t result = m5gm_module_set_power_off();
    CHECK_EQUAL(-1, result);
}


TEST(lib_m5gm, Test_m5gm_module_set_power_cycle)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "255");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 0 > /sys/class/leds/5G_rst/brightness")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 255 > /sys/class/leds/5G_rst/brightness")->andReturnIntValue(0);
    int32_t result = m5gm_module_set_power_cycle();
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_cycle_fail1)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "255");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 0 > /sys/class/leds/5G_rst/brightness")->andReturnIntValue(-1);
    int32_t result = m5gm_module_set_power_cycle();
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_cycle_fail2)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "255");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);

    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 0 > /sys/class/leds/5G_rst/brightness")->andReturnIntValue(0);
    mock_c()->expectOneCall("system")->withStringParameters("command", "echo 255 > /sys/class/leds/5G_rst/brightness")->andReturnIntValue(-1);

    int32_t result = m5gm_module_set_power_cycle();
    CHECK_EQUAL(-3, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_cycle_fail3)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "255");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "0");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);
    
    int32_t result = m5gm_module_set_power_cycle();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_cycle_fail4)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "0");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);
    
    int32_t result = m5gm_module_set_power_cycle();
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_module_set_power_cycle_fail5)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "0");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "0");
    fseek(output2, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);
    
    int32_t result = m5gm_module_set_power_cycle();
    CHECK_EQUAL(-1, result);
}


TEST(lib_m5gm, Test_m5gm_module_power_down_no_lock)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QPOWD=0", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_power_down_no_lock(IMMEDIATE_POWER_DOWN);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test_m5gm_module_power_down_no_lock2)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QPOWD=1", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "OK\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_power_down_no_lock(NORMAL_POWER_DOWN);
    CHECK_EQUAL(0, result);
}


TEST(lib_m5gm, Test_m5gm_module_power_down_no_lock_fail1)
{
    const char * const argv[] = {"/usr/sbin/mxat", "-d", "/dev/ttyUSB3", "-c", "AT+QPOWD=1", "-t", "10000", NULL};
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "ERROR\r\n");
    fseek(output, 0, SEEK_SET);
    mock_c()->expectOneCall("moxa_popen")\
        ->withParameterOfType("StringArray", "argv", (void*)argv)\
        ->withStringParameters("type", "r")\
        ->andReturnPointerValue(output);
    int32_t result = m5gm_module_power_down_no_lock(NORMAL_POWER_DOWN);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test_m5gm_module_power_down_no_lock_fail2)
{
    int32_t result = m5gm_module_power_down_no_lock(2);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_power_status)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "255");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);
    FILE * output3 = fopen("./test/output3","w+");
    fprintf(output3, "255");
    fseek(output3, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_rst/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output3);

    int32_t result = m5gm_module_get_power_status();
    CHECK_EQUAL(CELLULAR_MODULE_POWER_ON, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_power_status2)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "0");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "0");
    fseek(output2, 0, SEEK_SET);
    FILE * output3 = fopen("./test/output3","w+");
    fprintf(output3, "255");
    fseek(output3, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_rst/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output3);

    int32_t result = m5gm_module_get_power_status();
    CHECK_EQUAL(CELLULAR_MODULE_POWER_OFF, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_power_status3)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "255");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);
    FILE * output3 = fopen("./test/output3","w+");
    fprintf(output3, "0");
    fseek(output3, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_rst/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output3);

    int32_t result = m5gm_module_get_power_status();
    CHECK_EQUAL(CELLULAR_MODULE_POWER_CYCLE, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_power_status4)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "255");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "0");
    fseek(output2, 0, SEEK_SET);
    FILE * output3 = fopen("./test/output3","w+");
    fprintf(output3, "255");
    fseek(output3, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_rst/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output3);

    int32_t result = m5gm_module_get_power_status();
    CHECK_EQUAL(CELLULAR_MODULE_POWER_STATUS_UNKNOWN, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_power_status5)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "0");
    fseek(output, 0, SEEK_SET);
    FILE * output2 = fopen("./test/output2","w+");
    fprintf(output2, "255");
    fseek(output2, 0, SEEK_SET);
    FILE * output3 = fopen("./test/output3","w+");
    fprintf(output3, "255");
    fseek(output3, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_rst/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output3);

    int32_t result = m5gm_module_get_power_status();
    CHECK_EQUAL(CELLULAR_MODULE_POWER_STATUS_UNKNOWN, result);
}

TEST(lib_m5gm, Test_m5gm_module_get_power_status6)
{
    FILE * output = NULL;
    FILE * output2 = NULL;
    FILE * output3 = NULL;

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_pwr/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output2);
    
    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_rst/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output3);

    int32_t result = m5gm_module_get_power_status();
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_get_value)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "0");
    fseek(output, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    int32_t result = _m5gm_gpio_get_value(GPIO_FULL_CARD_POWER_OFF);
    CHECK_EQUAL(0, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_get_value2)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "253");
    fseek(output, 0, SEEK_SET);

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    int32_t result = _m5gm_gpio_get_value(GPIO_FULL_CARD_POWER_OFF);
    CHECK_EQUAL(253, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_get_value_fail)
{
    int32_t result = _m5gm_gpio_get_value(NULL);
    CHECK_EQUAL(-1, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_get_value_fail2)
{
    FILE * output = NULL;

    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    int32_t result = _m5gm_gpio_get_value(GPIO_FULL_CARD_POWER_OFF);
    CHECK_EQUAL(-2, result);
}

TEST(lib_m5gm, Test__m5gm_gpio_get_value_fail3)
{
    FILE * output = fopen("./test/output","w+");
    fprintf(output, "XXX");
    fseek(output, 0, SEEK_SET);


    mock_c()->expectOneCall("moxa_fopen")\
        ->withStringParameters("filename", "/sys/class/leds/5G_en/brightness")\
        ->withStringParameters("mode", "r")\
        ->andReturnPointerValue(output);
    
    int32_t result = _m5gm_gpio_get_value(GPIO_FULL_CARD_POWER_OFF);
    CHECK_EQUAL(0, result);
}
