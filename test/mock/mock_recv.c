#include "CppUTestExt/MockSupport_c.h"
#include <sys/select.h>
#include <sys/socket.h>

ssize_t __wrap_recv(int sockfd, void *buf, size_t len, int flags)
{
    return mock_c()->actualCall("recv")
                   ->withOutputParameter("buf", buf)
                   ->longIntReturnValue();
}
