#include "CppUTestExt/MockSupport_c.h"
#include <sys/select.h>
#include <sys/socket.h>

ssize_t __wrap_send(int sockfd, const void *buf, size_t len, int flags)
{
    return mock_c()->actualCall("send")
                   ->withStringParameters("buf", (const char*)buf)
                   ->longIntReturnValue();
}
