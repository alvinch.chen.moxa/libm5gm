#include "CppUTestExt/MockSupport_c.h"
#include <sys/select.h>
#include <sys/socket.h>
#include <stdio.h>
#include <pthread.h>

int  __wrap_system(const char *command)
{
    return mock_c()->actualCall("system")\
                   ->withStringParameters("command", (const char*)command)\
                   ->intReturnValue();
}

FILE *  __wrap_moxa_popen(char * const argv[], const char *type)
{

    return (FILE *)(mock_c()->actualCall("moxa_popen")
                    ->withParameterOfType("StringArray", "argv", (void*)argv)
                    ->withStringParameters("type", type)
                    ->pointerReturnValue());
}

FILE * __wrap_moxa_fopen(const char *filename, const char *mode)
{
    return (FILE *)(mock_c()->actualCall("moxa_fopen")
                    ->withStringParameters("filename", filename)
                    ->withStringParameters("mode", mode)
                    ->pointerReturnValue());

}

int   __wrap_pclose(FILE * fd)
{
    return 0;
}

int  __wrap_pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg)
{

    return (mock_c()->actualCall("pthread_create")
                    ->withPointerParameters("start_routine", (void*) start_routine)
                    ->intReturnValue());
}

int  __wrap_pthread_join(pthread_t thread, void **retval)
{

    return (mock_c()->actualCall("pthread_join")
                    ->withUnsignedLongIntParameters("thread", (unsigned long int) thread)
                    ->withPointerParameters("retval", (void*) retval)
                    ->intReturnValue());
}

void  __wrap_pthread_exit(void * retval)
{

}

