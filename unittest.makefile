CC=gcc
PWD=$(shell pwd)
CPPUTEST_HOME=$(shell pwd)/external/cpputest-3.8

SILENCE=@
COMPONENT_NAME = libm5gm

CPP_PLATFORM = Gcc
PROJECT_HOME_DIR = .

SRC_DIRS = src

SRC_FILES=

TEST_SRC_DIRS = test

TEST_SRC_FILES=

INCLUDE_DIRS = \
	$(PWD)/external/ \
	$(CPPUTEST_HOME)/include \
	$(PWD)/include

MOCKS_SRC_DIRS= test/mock

CPPUTEST_CFLAGS+= -DPOSIX -Wno-c++14-compat -Wno-unused-parameter
CPPUTEST_CXXFLAGS+= -DPOSIX -Wno-c++14-compat -Wno-unused-parameter
CPPUTEST_CPPFLAGS+= -DPOSIX -Wno-c++14-compat -Wno-unused-parameter
CPPUTEST_LDFLAGS+= $$(objdump -t $$(find $(PWD)/objs -type f -name *.o) \
		| grep ".text.*__wrap_" \
		| sed -e "s/.*.text.*__wrap_\(.*\)/-Wl,--wrap=\1/")

CPPUTEST_USE_EXTENSIONS=Y
CPPUTEST_WARNINGFLAGS=
CPPUTEST_USE_GCOV=Y
GCOV_ARGS=-b -c
CPPUTEST_PEDANTIC_ERRORS=N
CPPUTEST_EXE_FLAGS=-v

include $(CPPUTEST_HOME)/build/MakefileWorker.mk 
