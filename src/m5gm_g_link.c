/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/wait.h>

#include "lib_m5gm/lib_m5gm.h"

pthread_t g_link_thread_t;

int32_t g_link_thread_run = 0;

int32_t connection_enable = 0; // 1: start , 0: stop , 2: restart

char apn_name[MAX_NAME_LEN] = {0};

void * _g_link_thread(void *arg) 
{
    int32_t conn_status_ret = -1;
    int32_t ret = -1;
    u_int32_t fail_count = 0;
    u_int32_t loop_count = 0;
    struct g_link_thread_config * config = (struct g_link_thread_config *) arg;

    while(g_link_thread_run)
    {
        //check if connection is ok
        conn_status_ret = ! m5gm_g_link_ping(config->ping_host, config->ping_timeout);
        sleep(1);
        
        if(conn_status_ret < 0)
        {
            fail_count++;
            goto FAIL_RECOVERY;
        }
        
        if(conn_status_ret == 1 && connection_enable == 1)
        {
            fail_count = 0;
             continue;
        }
        
        if(conn_status_ret == 0 && connection_enable == 0)
        {
            fail_count = 0;
             continue;
        }
        
        if(conn_status_ret == 0 && connection_enable == 1)
        {
            ret = m5gm_conn_start(apn_name, 0);
            M5GM_DEBUG(M5GM_LOG_WARN, "[G_link] %s:%d m5gm_conn_start(%d) ret = %d\n", __func__, __LINE__, 0, ret);
            if(ret < 0)
            {
                fail_count++;
                goto FAIL_RECOVERY;
            }
            continue;
        }
        else if(conn_status_ret == 1 && connection_enable == 0)
        {
            ret = m5gm_conn_stop(0);
            M5GM_DEBUG(M5GM_LOG_WARN, "[G_link] %s:%d m5gm_conn_stop(%d) ret = %d\n", __func__, __LINE__, 0, ret);
            if(ret < 0)
            {
                fail_count++;
                goto FAIL_RECOVERY;
            }
            continue;
        }
        
        
       
        
FAIL_RECOVERY:
        if(fail_count >= config->hard_reset_threhold)
        {
            M5GM_DEBUG(M5GM_LOG_WARN, "[G_link]  %s:%d fail_count >= config->hard_reset_threhold\n", __func__, __LINE__);
            // kill process
            system("killall -9 qmicli");
            system("killall -9 mxat");
            ret = m5gm_module_set_power_cycle();
            M5GM_DEBUG(M5GM_LOG_WARN, "[G_link]  %s:%d m5gm_set_module_functionality(%d, %d) ret = %d\n", __func__, __LINE__, FULL_FUNCTIONALITY, RESET_UE, ret);
            loop_count = 0;
            fail_count = 0;
            while(m5gm_get_module_functionality() != 1)
            {
                sleep(1);
                loop_count++;
                if(loop_count > 15)
                    break;
            }
            
            continue;
        }
        if(fail_count == config->ue_reset_threhold)
        {
            M5GM_DEBUG(M5GM_LOG_WARN, "[G_link]  %s:%d fail_count >= config->ue_reset_threhold\n", __func__, __LINE__);
            ret = m5gm_set_module_functionality(FULL_FUNCTIONALITY, RESET_UE);
            M5GM_DEBUG(M5GM_LOG_WARN, "[G_link]  %s:%d m5gm_set_module_functionality(%d, %d) ret = %d\n", __func__, __LINE__, FULL_FUNCTIONALITY, RESET_UE, ret);
            loop_count = 0;
            while(m5gm_get_module_functionality() != 1)
            {
                sleep(1);
                loop_count++;
                if(loop_count > 15)
                    break;
            }
            continue;
        }
        if(fail_count == config->functional_reset_threhold)
        {
            M5GM_DEBUG(M5GM_LOG_WARN, "[G_link]  %s:%d fail_count >= config->functional_reset_threhold\n", __func__, __LINE__);
            ret = m5gm_set_module_functionality(MINIMUM_FUNCTIONALITY, DO_NOT_RESET);
            M5GM_DEBUG(M5GM_LOG_WARN, "[G_link] %s:%d m5gm_set_module_functionality(%d, %d) ret = %d\n", __func__, __LINE__, MINIMUM_FUNCTIONALITY, DO_NOT_RESET, ret);
            ret = m5gm_set_module_functionality(FULL_FUNCTIONALITY, DO_NOT_RESET);
            M5GM_DEBUG(M5GM_LOG_WARN, "[G_link] %s:%d m5gm_set_module_functionality(%d, %d) ret = %d\n", __func__, __LINE__, FULL_FUNCTIONALITY, DO_NOT_RESET, ret);
            loop_count = 0;
            while(m5gm_get_module_functionality() != 1)
            {
                sleep(1);
                loop_count++;
                if(loop_count > 5)
                    break;
            }
            // it seems we have to wait about 2 sec even the functionality shows 1, otherwise connect will fail.
            sleep(3);
            continue;
        }
        if(fail_count == config->connection_reset_threhold)
        {
            M5GM_DEBUG(M5GM_LOG_WARN, "[G_link]  %s:%d fail_count >= config->connection_reset_threhold\n", __func__, __LINE__);
            m5gm_conn_stop(0);
            if(connection_enable == 1)
            {
                ret = m5gm_conn_start(apn_name, 0);
                M5GM_DEBUG(M5GM_LOG_WARN, "[G_link] %s:%d m5gm_conn_stop -> m5gm_conn_start (%d) ret = %d\n", __func__, __LINE__, 0, ret);
            }
            continue;
        }
    };
    pthread_exit(NULL);
    return NULL;
}

int32_t m5gm_g_link_ping(const char * host, u_int32_t timeout)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char timeout_str[8] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    if(!host)
        return -2;
    
    if(timeout > 3)
        return -3;
    
    snprintf(timeout_str, 8, "%d", timeout);
    const char * const command_list[] = {"/bin/ping", "-W", timeout_str, "-c", "1", host, NULL};
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_DEBUG ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strncmp(result_buffer, "1 packets transmitted, 1 packets received, 0% packet loss\n", strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    return ret;
}


int32_t m5gm_g_link_connection_start(void)
{
    M5GM_DEBUG(M5GM_LOG_DEBUG, "[G_link] %s:%d m5gm_g_link_connection_start\n", __func__, __LINE__);
    int32_t fd = M5GM_LOCK();
    connection_enable = 1;
    M5GM_UNLOCK(fd);
    return 0;
}

int32_t m5gm_g_link_connection_stop(void)
{
    M5GM_DEBUG(M5GM_LOG_DEBUG, "[G_link] %s:%d m5gm_g_link_connection_stop\n", __func__, __LINE__);
    int32_t fd = M5GM_LOCK();
    connection_enable = 0;
    M5GM_UNLOCK(fd);
    return 0;
}

int32_t m5gm_g_link_set_apn_name(const char * name)
{
    int32_t fd = M5GM_LOCK();
    snprintf(apn_name, MAX_NAME_LEN, "%s", name);
    M5GM_UNLOCK(fd);
    return 0;
}

int32_t m5gm_g_link_thread_start(struct g_link_thread_config config)
{
    int32_t ret;
    static struct g_link_thread_config g_config;
    g_config = config;
    g_link_thread_run = 1;
    ret = pthread_create(&g_link_thread_t, NULL, _g_link_thread, (void *) &g_config);
    return ret;
}
    
int32_t m5gm_g_link_thread_stop(void)
{
    int32_t ret = -1;
    if(g_link_thread_run == 0)
        return 0;
    
    g_link_thread_run = 0;
    ret = pthread_join(g_link_thread_t, NULL);
    return ret;
}
