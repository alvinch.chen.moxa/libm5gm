/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */
#include "lib_m5gm/lib_m5gm.h"
#include <sys/file.h>
#include <unistd.h>

int32_t m5gm_lock(void)
{
    int32_t lock_fd = open(LIB_M5GM_LOCK_PATH, O_WRONLY|O_CREAT);
    if(lock_fd == -1)
        return -1;
    if(flock(lock_fd, LOCK_EX) != 0)
    {
        close(lock_fd);
        return -1;
    }
    return lock_fd;
}

int32_t m5gm_unlock(int32_t lock_fd)
{
    int32_t ret = flock(lock_fd, LOCK_UN);
    if(ret != 0)
    {
        close(lock_fd);
        return ret;
    }
    return close(lock_fd);
}

