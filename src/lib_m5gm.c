/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */
#include "lib_m5gm/lib_m5gm.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/file.h>
#include <pthread.h>
#include <sys/wait.h>

char WWAN_DEV[MAX_PATH_LEN] = "cell_wan";
char QMICLI[MAX_PATH_LEN] = "/usr/bin/qmicli";
char QMI_NETWORK_PATH[MAX_PATH_LEN] = "/usr/bin/qmi-network";
char M5GM_CONTROL_PATH[MAX_PATH_LEN] = "/home/work/alvin/m5Gm_control.sh";
char CDC_WDM[MAX_PATH_LEN] = "/dev/cdc-wdm0";
char DHCPCD[MAX_PATH_LEN] = "/sbin/udhcpc";
char IFCONFIG[MAX_PATH_LEN] = "/sbin/ifconfig";
char AT_DEV[MAX_PATH_LEN] = "/dev/ttyUSB3";
char MXAT_PATH[MAX_PATH_LEN] = "/usr/sbin/mxat";
char QFIREHOSE_PATH[MAX_PATH_LEN] = "/usr/bin/QFirehose";

static u_int32_t upgrade_percent = 0;

//function api
int32_t m5gm_init(struct m5gm_path_config config)
{
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    if(strlen(config.WWAN_DEV))
        snprintf(WWAN_DEV, MAX_PATH_LEN, config.WWAN_DEV);
    if(strlen(config.QMICLI))
        snprintf(QMICLI, MAX_PATH_LEN, config.QMICLI);
    if(strlen(config.QMI_NETWORK_PATH))
        snprintf(QMI_NETWORK_PATH, MAX_PATH_LEN, config.QMI_NETWORK_PATH);
    if(strlen(config.M5GM_CONTROL_PATH))
        snprintf(M5GM_CONTROL_PATH, MAX_PATH_LEN, config.M5GM_CONTROL_PATH);
    if(strlen(config.CDC_WDM))
        snprintf(CDC_WDM, MAX_PATH_LEN, config.CDC_WDM);
    if(strlen(config.DHCPCD))
        snprintf(DHCPCD, MAX_PATH_LEN, config.DHCPCD);
    if(strlen(config.IFCONFIG))
        snprintf(IFCONFIG, MAX_PATH_LEN, config.IFCONFIG);
    if(strlen(config.AT_DEV))
        snprintf(AT_DEV, MAX_PATH_LEN, config.AT_DEV);
    if(strlen(config.MXAT_PATH))
        snprintf(MXAT_PATH, MAX_PATH_LEN, config.MXAT_PATH);
    if(strlen(config.QFIREHOSE_PATH))
        snprintf(QFIREHOSE_PATH, MAX_PATH_LEN, config.QFIREHOSE_PATH);
    M5GM_UNLOCK(fd);
    return 0;
}

int32_t m5gm_release_qmi_all_client_id(void)
{
    int32_t fd = M5GM_LOCK();
    u_long i;
    for(i = 15; i < 256; i++)
    {
        M5GM_SYSTEM_WITH_LOG("/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%lu", i);
    }
    M5GM_UNLOCK(fd);
    return 0;
}

int32_t _m5gm_save_conn_state(const char * file_path, u_long pdh, u_long cid)
{
    FILE * state_file = fopen(file_path,"w+");
    if(!state_file)
        return -1;
    if(fprintf(state_file, "PDH=%lu\n", pdh) < 0)
        return -2;
    if(fprintf(state_file, "CID=%lu\n", cid) < 0)
        return -3;
    fclose(state_file);
    return 0;
}

int32_t _m5gm_load_conn_state(const char * file_path, u_long *pdh, u_long *cid)
{
    FILE * state_file = fopen(file_path,"r");
    if(!state_file)
    {
        return -1;
    }
    if(fscanf(state_file, "PDH=%lu\n", pdh) <= 0)
        return -2;
    if(fscanf(state_file, "CID=%lu\n", cid) <= 0)
        return -3;
    fclose(state_file);
    return 0;
}

int32_t _m5gm_start_network_no_lock(const char *apn)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char arg_string[512] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    u_long pdh = 0;
    u_long cid = 0;
    int32_t is_pdh_set = 0;
    int32_t is_cid_set = 0;
    int32_t is_network_started = 0;
    if(!apn)
    {
        return ret;
    }
    
    snprintf(arg_string, sizeof(arg_string), "--wds-start-network=apn=%s,ip-type=4,3gpp-profile=1", apn);
    
    const char * const command_list[] = {QMICLI, "-d", CDC_WDM, arg_string, "--client-no-release-cid", NULL};
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_DEBUG ,"%s\n",result_buffer);
        if(is_pdh_set == 0 && sscanf(result_buffer, "        Packet data handle: '%lu'", &pdh)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"pdh = %lu \n", pdh );
            is_pdh_set = 1;
            continue;
        }
        if(is_cid_set == 0 && sscanf(result_buffer, "            CID: '%lu'", &cid)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"cid = %lu \n", cid );
            is_cid_set = 1;
            continue;
        }
        if(is_network_started == 0 && strstr(result_buffer, "Network started")!=NULL) {
            is_network_started = 1;
            continue;
        }
    }
    
    if(is_pdh_set && is_cid_set && is_network_started)
    //if(is_pdh_set && is_network_started)
        ret = 0;
    else
    {
        if(is_cid_set)
            M5GM_SYSTEM_WITH_LOG("/usr/bin/qmicli -d /dev/cdc-wdm0 --wds-noop  --client-cid=%lu", cid);
        return -1;
    }
    if(_m5gm_save_conn_state(CONN_STATE_FILE_PATH, pdh, cid))
        return -2;
    waitpid(pid, NULL, 0);
    pclose(fp);
    return ret;
}

int32_t _m5gm_stop_network_no_lock(void)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char pdh_arg_string[128] = {0};
    char cid_arg_string[128] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    u_long pdh = 0;
    u_long cid = 0;
    int32_t is_network_stopped = 0;
    
    if(_m5gm_load_conn_state(CONN_STATE_FILE_PATH, &pdh, &cid))
        return ret;
    
    snprintf(pdh_arg_string, sizeof(pdh_arg_string), "--wds-stop-network=%lu", pdh);
    snprintf(cid_arg_string, sizeof(cid_arg_string), "--client-cid=%lu", cid);
    
    const char * const command_list[] = {QMICLI, "-d", CDC_WDM, pdh_arg_string, cid_arg_string, NULL};
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_DEBUG ,"%s\n",result_buffer);
        if(is_network_stopped == 0 && strstr(result_buffer, "Network stopped")!=NULL) {
            ret = 0;
            break;
        }
    }
    
    waitpid(pid, NULL, 0);
    pclose(fp);
    return ret;
}


int32_t m5gm_conn_start(const char *apn, u_int32_t pdp)
{
    int32_t ret = 0;
    if(!apn)
    {
        return -1;
    }
    //force release connection
    m5gm_conn_stop(0);
    //force release client id to prevent client id leaks.
    m5gm_release_qmi_all_client_id();
    
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    M5GM_SYSTEM_WITH_LOG("kill `cat /var/run/udhcpc-%s.pid`", WWAN_DEV);
    
    ret = M5GM_SYSTEM_WITH_LOG("%s %s down", IFCONFIG, WWAN_DEV);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -2;
    }
    ret = M5GM_SYSTEM_WITH_LOG("echo Y > /sys/class/net/%s/qmi/raw_ip", WWAN_DEV);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -3;
    }
    ret = M5GM_SYSTEM_WITH_LOG("%s %s up", IFCONFIG, WWAN_DEV);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -4;
    }
    ret = _m5gm_start_network_no_lock(apn);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -5;
    }
    ret = M5GM_SYSTEM_WITH_LOG("%s -n -i %s -p /var/run/udhcpc-%s.pid", DHCPCD, WWAN_DEV, WWAN_DEV);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -6;
    }
    M5GM_UNLOCK(fd);
    return 0;
}

int32_t m5gm_conn_stop(u_int32_t pdp)
{
    int32_t fd = M5GM_LOCK();
    int32_t ret = 0;
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    ret = M5GM_SYSTEM_WITH_LOG("kill `cat /var/run/udhcpc-%s.pid`", WWAN_DEV);
    //if(ret)
    //{
    //    M5GM_UNLOCK(fd);
    //    return -2;
    //}
    
    ret = M5GM_SYSTEM_WITH_LOG("%s %s down", IFCONFIG, WWAN_DEV);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -3;
    }
    
    ret = _m5gm_stop_network_no_lock();
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -1;
    }
    
    M5GM_UNLOCK(fd);
    return 0;
}

int32_t m5gm_conn_get_status(void)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t ret = -1;
    int32_t pid = 0;
    const char * const command_list[] = {QMICLI, "-d", CDC_WDM, "--wds-get-packet-service-status", NULL};
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE, "\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strstr(result_buffer, "status: 'connected'")!=NULL) {
            ret = 1;
            break;
        }
        else if(strstr(result_buffer, "status: 'disconnected'")!=NULL) {
            ret = 0;
            break;
        }
    }
    waitpid(pid,NULL,0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_conn_lte_tracking_area_code(void)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t ret = -1;
    int32_t pid = 0;

    const char * const command_list[] = {QMICLI, "-d", CDC_WDM, "--nas-get-serving-system", NULL};
    const char *format = "        LTE tracking area code: '%d'";
    int32_t lte_tracking_area_code;
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(sscanf(result_buffer, format, &lte_tracking_area_code )) {
            ret = lte_tracking_area_code;
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"LTE tracking area code = %d \n", lte_tracking_area_code );
            break;
        }
    }
    waitpid(pid,NULL,0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t _m5gm_conn_cs_attach_status(void)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t ret = -1;
    int32_t pid = 0;
    
    const char * const command_list[] = {QMICLI, "-d", CDC_WDM, "--nas-get-serving-system", NULL};
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE , "\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strstr(result_buffer, "CS: 'attached'")!=NULL) {
            ret = 1;
            break;
        }
        else if(strstr(result_buffer, "CS: 'detached'")!=NULL) {
            ret = 0;
            break;
        }
    }
    waitpid(pid,NULL,0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t _m5gm_conn_ps_attach_status(void)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t ret = -1;
    int32_t pid = 0;
    //const char * const command_list[] = {QMICLI, "-d", CDC_WDM, "--nas-get-serving-system", NULL};
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+CGATT?", "-t", AT_COMMAND_TIMEOUT, NULL};

    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE , "\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strstr(result_buffer, "+CGATT: 1")!=NULL) {
            ret = 1;
            break;
        }
        else if(strstr(result_buffer, "+CGATT: 0")!=NULL) {
            ret = 0;
            break;
        }
    }
    waitpid(pid,NULL,0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_get_attach_status(void)
{
    return _m5gm_conn_ps_attach_status();
}


int32_t m5gm_op_at(const char *at_cmd, u_int32_t timeout)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char timeout_string[128] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    snprintf(timeout_string, sizeof(timeout_string), "%d", timeout);
    
    if(!at_cmd)
    {
        return ret;
    }
    
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", at_cmd, "-t", timeout_string, NULL};
    
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_DEBUG ,"%s\n",result_buffer);
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_get_carrier_info(struct carrier_info * info)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t ret = -1;
    int32_t pid = 0;
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+QSPN", "-t", AT_COMMAND_TIMEOUT, NULL};
    const char *format = "+QSPN: \"%[^\"]\",\"%[^\"]\",\"\",%d,\"%[^\"]\"";
    int32_t alphabet = 0;
    int32_t is_carrier_set = 0;
    if(!info)
    {
        return ret;
    }
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(sscanf(result_buffer, format, info->service_provider_full_name,
            info->service_provider_short_name,
            &alphabet, 
            info->registered_plmn)) 
        {
            is_carrier_set = 1;
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"service_provider_full_name = %s \n", info->service_provider_full_name);
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"service_provider_short_name = %s \n", info->service_provider_short_name);
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"alphabet = %d \n", alphabet);
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"registered_plmn = %s \n", info->registered_plmn);
            continue;
        }
        if(is_carrier_set && strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }

    }
    waitpid(pid,NULL,0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

int32_t m5gm_get_signal_status_qmi(struct signal_status *stat)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t ret = -1;
    int32_t signal_type = 0; //0 = no_signal , 1 = 3g , 2 = 4g , 3 = 5g
    int32_t pid = 0;
    const char * const command_list[] = {QMICLI, "-d", CDC_WDM, "-p", "--nas-get-signal-info", NULL};
    
    if(!stat)
    {
        return ret;
    }
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strstr(result_buffer, "5G:")!=NULL) {
            signal_type = 3;
            stat->is_5g = 1;
            ret = 0;
            continue;
        }
        if(signal_type == 3 && sscanf(result_buffer, "       RSRP: '%d dBm'", &stat->rsrp_5g)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"rsrp = %d \n", stat->rsrp_5g );
            continue;
        }
        if(signal_type == 3 && sscanf(result_buffer, "       RSRQ: '%d dBm'", &stat->rsrq_5g)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"rsrq = %d \n", stat->rsrq_5g );
            continue;
        }
        if(signal_type == 3 && sscanf(result_buffer, "       SNR: '%f dB'", &stat->snr_5g)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"snr = %f \n", stat->snr_5g );
            continue;
        }
        if(strstr(result_buffer, "LTE:")!=NULL) {
            signal_type = 2;
            stat->is_lte = 1;
            ret = 0;
            continue;
        }
        if(signal_type == 2 && sscanf(result_buffer, "       RSSI: '%d dBm'", &stat->lte_rssi)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"rssi = %d \n", stat->lte_rssi );
            continue;
        }
        if(signal_type == 2 && sscanf(result_buffer, "       RSRQ: '%d dB'", &stat->lte_rsrq)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"rsrq = %d \n", stat->lte_rsrq );
            continue;
        }
        if(signal_type == 2 && sscanf(result_buffer, "       RSRP: '%d dBm'", &stat->lte_rsrp)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"rsrp = %d \n", stat->lte_rsrp );
            continue;
        }
        if(signal_type == 2 && sscanf(result_buffer, "       SNR: '%f dB'", &stat->lte_snr)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"snr = %f \n", stat->lte_snr );
            continue;
        }
        if(strstr(result_buffer, "WCDMA:")!=NULL) {
            signal_type = 1;
            stat->is_wcdma = 1;
            ret = 0;
            continue;
        }
        if(signal_type == 1 && sscanf(result_buffer, "       RSSI: '%d dBm'", &stat->wcdma_rssi)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"rssi = %d \n", stat->wcdma_rssi );
            continue;
        }
        if(signal_type == 1 && sscanf(result_buffer, "       ECIO: '%f dBm'", &stat->wcdma_ecio)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"ecio = %f \n", stat->wcdma_ecio );
            continue;
        }

    }
    waitpid(pid,NULL,0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}
#if 1
int32_t m5gm_get_signal_status(struct signal_status *stat)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t ret = -1;
    int32_t pid = 0;

    char cellid[MAX_NAME_LEN] = {0};

    char rsrq_5g[MAX_NAME_LEN] = {0};
    char rsrp_5g[MAX_NAME_LEN] = {0};
    char snr_5g[MAX_NAME_LEN] = {0};

    char lte_rssi[MAX_NAME_LEN] = {0};
    char lte_rsrq[MAX_NAME_LEN] = {0};
    char lte_rsrp[MAX_NAME_LEN] = {0};
    char lte_snr[MAX_NAME_LEN] = {0};
    
    char wcdma_rssi[MAX_NAME_LEN] = {0};
    char wcdma_ecio[MAX_NAME_LEN] = {0};

    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "at+qeng=\"servingcell\"", "-t", AT_COMMAND_TIMEOUT, NULL};
    if(!stat)
    {
        return ret;
    }
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;

    //+QENG: "LTE","FDD",466,97,386798F,17,275,1,4,4,2CC4,-85,-7,-59,17,255,-32768,42
    //+QENG:"NR5G-NSA",466,97,65535,-32768,-32768,-32768

    const char *endc_lte_format = "+QENG: \"LTE\",%*[^,],%*[^,],%*[^,],%[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%[^,],%[^,],%[^,],%[^,],%*[^,],%*[^,],%*[^,]";
    const char *endc_5g_nsa_format = "+QENG:\"NR5G-NSA\",%*[^,],%*[^,],%*[^,],%[^,],%[^,],%[^,]";//-32768 = no signal
    //+QENG: "servingcell","NOCONN","WCDMA",466,97,2B6A,921037,10738,328,59,-75,-4,-,-,-,-,-

    //+QENG: "servingcell","NOCONN","LTE","FDD",466,97,3867985,17,1275,3,4,4,2CC4,-83,-14,-50,13,255,170,-
    const char *lte_format = "+QENG: \"servingcell\",%*[^,],\"LTE\",%*[^,],%*[^,],%*[^,],%[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%[^,],%[^,],%[^,],%[^,],%*[^,],%*[^,],%*[^,]";
    //+QENG: "servingcell","NOCONN","WCDMA",466,97,2B6A,921037,10738,328,59,-74,-3,-,-,-,-,-
    const char *wcdma_format = "+QENG: \"servingcell\",%*[^,],\"WCDMA\",%*[^,],%*[^,],%*[^,],%[^,],%*[^,],%*[^,],%*[^,],%[^,],%[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,]";

    fp = moxa_popen(command_list, "r", &pid);

    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(sscanf(result_buffer, endc_lte_format,
            cellid,
            lte_rsrp,
            lte_rsrq,
            lte_rssi,
            lte_snr) == 5) 
        {
            //printf("nsa lte\n");
            stat->cellid = atoi(cellid);
            stat->lte_rsrq = atoi(lte_rsrq);
            stat->lte_rsrp = atoi(lte_rsrp);
            stat->lte_rssi = atoi(lte_rssi);
            stat->lte_snr = (float) atoi(lte_snr);
            stat->is_lte = 1;
            continue;
        }
        if(sscanf(result_buffer, endc_5g_nsa_format,
            rsrp_5g,
            snr_5g,
            rsrq_5g) == 3) 
        {
            //printf("nsa 5g\n");
            if(atoi(rsrq_5g) != -32768)
            {
                stat->rsrq_5g = atoi(rsrq_5g);
                stat->rsrp_5g = atoi(rsrp_5g);
                stat->snr_5g = (float) atoi(snr_5g);
                stat->is_5g = 1;
            }
            continue;
        }
        if(sscanf(result_buffer, lte_format,
            cellid,
            lte_rsrp,
            lte_rsrq,
            lte_rssi,
            lte_snr) == 5) 
        {
            
            //printf("lte \n");
            stat->cellid = atoi(cellid);
            stat->lte_rsrq = atoi(lte_rsrq);
            stat->lte_rsrp = atoi(lte_rsrp);
            stat->lte_rssi = atoi(lte_rssi);
            stat->lte_snr = (float) atoi(lte_snr);
            stat->is_lte = 1;
            continue;
        }
        if(sscanf(result_buffer, wcdma_format,
            cellid,
            wcdma_rssi,
            wcdma_ecio) == 3) 
        {
            //printf("wcdma\n");
            stat->cellid = atoi(cellid);
            stat->wcdma_rssi = atoi(wcdma_rssi);
            stat->wcdma_ecio = (float) atoi(wcdma_ecio);
            stat->is_wcdma = 1;
            continue;
        }

        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }

    waitpid(pid,NULL,0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}
#endif
int32_t m5gm_module_get_info(struct module_info * info)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "ATI", "-t", AT_COMMAND_TIMEOUT, NULL};
    const char *manufacturer_format = "%s";
    const char *module_name_format = "%s";
    const char *firmware_version_format = "Revision: %s";
    int32_t is_first_string_skipped = 0;
    int32_t is_manufacturer_set = 0;
    int32_t is_module_name_set = 0;
    int32_t is_firmware_version_set = 0;
    if(!info)
    {
        return ret;
    }
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(is_first_string_skipped == 0) {
            is_first_string_skipped = 1;
            continue;
        }
        if(is_manufacturer_set == 0 && sscanf(result_buffer, manufacturer_format, info->manufacturer)) {
            is_manufacturer_set = 1;
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"manufacturer = %s \n", info->manufacturer);
            continue;
        }
        if(is_module_name_set == 0 && sscanf(result_buffer, module_name_format, info->module_name)) {
            is_module_name_set = 1;
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"module_name = %s \n", info->module_name);
            continue;
        }
        if(sscanf(result_buffer, firmware_version_format, info->firmware_version)) {
            is_firmware_version_set = 1;
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"firmware_version = %s \n", info->firmware_version);
            continue;
        }
        if(is_manufacturer_set && is_module_name_set && is_firmware_version_set && strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
        
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

int32_t m5gm_get_imei(char * imei)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t ret = -1;
    int32_t pid = 0;
    const char * const command_list[] = {QMICLI, "-d", CDC_WDM, "-p", "--dms-get-ids", NULL};
    const char *format = "        IMEI: \'%[^\',]\'";
    if(!imei)
    {
        return ret;
    }
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(sscanf(result_buffer, format, imei)) {
            ret = 0;
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"imei = %s \n", imei);
            break;
        }
    }
    waitpid(pid,NULL,0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

int32_t m5gm_get_operational_mode(void)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t ret = -1;
    int32_t pid = 0;
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+CFUN?", "-t", AT_COMMAND_TIMEOUT, NULL};
    //const char * const command_list[] = {QMICLI, "-d", CDC_WDM, "-p", "--dms-get-operating-mode", NULL};
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strstr(result_buffer, "+CFUN: 1")!=NULL) {
            ret = OP_MODE_ONLINE;
            break;
        }
        else if(strstr(result_buffer, "+CFUN: 7")!=NULL) {
            ret = OP_MODE_OFFLINE;
            break;
        }
        //else if(strstr(result_buffer, "Mode: 'reset'")!=NULL) {
        //    ret = OP_MODE_RESET;
        //    break;
        //}
        else if(strstr(result_buffer, "+CFUN: 0")!=NULL) {
            ret = OP_MODE_LOW_POWER;
            break;
        }
        else if(strstr(result_buffer, "+CFUN: 4")!=NULL) {
            ret = OP_MODE_LOW_POWER;
            break;
        }
    }
    
    waitpid(pid,NULL,0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

int32_t _m5gm_set_operating_mode_qmi(const char * operating_mode)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t ret = -1;
    int32_t pid = 0;
    char qmi_command[512] = {0};
    
    if(!operating_mode)
        return -2;
    
    snprintf(qmi_command, sizeof(qmi_command), "--dms-set-operating-mode=%s", operating_mode);
    
    const char * const command_list[] = {QMICLI, "-d", CDC_WDM, "-p", qmi_command, NULL};
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strstr(result_buffer, "Operating mode set successfully")!=NULL) {
            ret = 0;
            break;
        }
    }
    waitpid(pid,NULL,0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

/*
    Online – Indicates that the device can acquire a system and make calls
*/
int32_t _m5gm_set_operating_mode_online(void)
{
    return m5gm_set_module_functionality(FULL_FUNCTIONALITY, DO_NOT_RESET);

}
/*
    Offline – Phone has deactivated RF and partially shutdown; the device must be power cycled before it
    can reacquire service from this mode
*/
int32_t _m5gm_set_operating_mode_offline(void)
{
    return _m5gm_set_operating_mode_qmi("offline");

}

/*
    Device is in the process of power cycling
    the only transition allowed out of "offline" is "reset" (i.e. power cycle the modem).
*/
int32_t _m5gm_set_operating_mode_reset(void)
{
    return _m5gm_set_operating_mode_qmi("reset");
}
/*
    "low-power" (will do IMSI detach and RF off), if the module
    resets the setting is forgotten (i.e. it goes to "online"
    automatically)
*/
int32_t _m5gm_set_operating_mode_low_power(void)
{
    return m5gm_set_module_functionality(DISABLE_TX_RX, DO_NOT_RESET);
}
/*
    "persistent-low-power" (will do IMSI detach and RF off), and if
    the module resets the setting is re-applied (i.e. it doesn't go to
    "online" automatically)
*/
int32_t _m5gm_set_operating_mode_persistent_low_power(void)
{
    return _m5gm_set_operating_mode_qmi("persistent-low-power");
}


int32_t m5gm_set_flight_mode(u_int32_t enable)
{
    int32_t ret;
    if(enable)
    {
        //force release connection first
        m5gm_conn_stop(0);
        //force release client id to prevent client id leaks.
        m5gm_release_qmi_all_client_id();
        ret = _m5gm_set_operating_mode_low_power();
    }
    else
        ret = _m5gm_set_operating_mode_online();
    return ret;
}

int32_t _m5gm_get_imsi(char * imsi)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+CIMI", "-t", AT_COMMAND_TIMEOUT, NULL};
    const char *imsi_format = "%s";
    int32_t is_first_string_skipped = 0;
    int32_t is_imsi_set = 0;
    if(!imsi)
    {
        return ret;
    }
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(is_first_string_skipped == 0) {
            is_first_string_skipped = 1;
            continue;
        }
        if(is_imsi_set == 0 && sscanf(result_buffer, imsi_format, imsi)) {
            is_imsi_set = 1;
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"imsi = %s \n", imsi);
            continue;
        }
        if(is_imsi_set == 1 && strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

int32_t _m5gm_get_iccid(char * iccid)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+ICCID", "-t", AT_COMMAND_TIMEOUT, NULL};
    const char *iccid_format = "+ICCID: %s";
    int32_t is_first_string_skipped = 0;
    int32_t is_iccid_set = 0;
    if(!iccid)
    {
        return ret;
    }
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(is_first_string_skipped == 0) {
            is_first_string_skipped = 1;
            continue;
        }
        if(is_iccid_set == 0 && sscanf(result_buffer, iccid_format, iccid)) {
            is_iccid_set = 1;
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"iccid = %s \n", iccid);
            continue;
        }
        if(is_iccid_set == 1 && strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

int32_t m5gm_sim_get_info(struct sim_info * info)
{
    if(!info)
        return -1;
    if(_m5gm_get_imsi(info->imsi))
        return -1;

    if(_m5gm_get_iccid(info->iccid))
        return -1;
    
    return 0;
}

int32_t m5gm_module_get_temp(struct module_temperature * temperature)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+QTEMP", "-t", AT_COMMAND_TIMEOUT, NULL};
    const char *qfe_wtr_pa0_format = "+QTEMP:\"qfe_wtr_pa0\",\"%d\"";
    const char *aoss0_usr_format = "+QTEMP:\"aoss0-usr\",\"%d\"";
    const char *mdm_q6_usr_format = "+QTEMP:\"mdm-q6-usr\",\"%d\"";
    const char *ipa_usr_format = "+QTEMP:\"ipa-usr\",\"%d\"";
    const char *cpu0_a7_usr_format = "+QTEMP:\"cpu0-a7-usr\",\"%d\"";
    const char *mdm_5g_usr_format = "+QTEMP:\"mdm-5g-usr\",\"%d\"";
    const char *mdm_core_usr_format = "+QTEMP:\"mdm-core-usr\",\"%d\"";
    const char *mdm_vpe_usr_format = "+QTEMP:\"mdm-vpe-usr\",\"%d\"";
    const char *xo_therm_usr_format = "+QTEMP:\"xo-therm-usr\",\"%d\"";
    const char *sdx_case_therm_usr_format = "+QTEMP:\"sdx-case-therm-usr\",\"%d\"";
    if(!temperature)
    {
        return ret;
    }
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(sscanf(result_buffer, qfe_wtr_pa0_format, &temperature->qfe_wtr_pa0)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"qfe_wtr_pa0 = %d \n", temperature->qfe_wtr_pa0);
            continue;
        }
        if(sscanf(result_buffer, aoss0_usr_format, &temperature->aoss0_usr)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"aoss0_usr = %d \n", temperature->aoss0_usr);
            continue;
        }
        if(sscanf(result_buffer, mdm_q6_usr_format, &temperature->mdm_q6_usr)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"mdm_q6_usr = %d \n", temperature->mdm_q6_usr);
            continue;
        }
        if(sscanf(result_buffer, ipa_usr_format, &temperature->ipa_usr)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"ipa_usr = %d \n", temperature->ipa_usr);
            continue;
        }
        if(sscanf(result_buffer, cpu0_a7_usr_format, &temperature->cpu0_a7_usr)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"cpu0_a7_usr = %d \n", temperature->cpu0_a7_usr);
            continue;
        }
        if(sscanf(result_buffer, mdm_5g_usr_format, &temperature->mdm_5g_usr)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"mdm_5g_usr = %d \n", temperature->mdm_5g_usr);
            continue;
        }
        if(sscanf(result_buffer, mdm_core_usr_format, &temperature->mdm_core_usr)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"mdm_core_usr = %d \n", temperature->mdm_core_usr);
            continue;
        }
        if(sscanf(result_buffer, mdm_vpe_usr_format, &temperature->mdm_vpe_usr)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"mdm_vpe_usr = %d \n", temperature->mdm_vpe_usr);
            continue;
        }
        if(sscanf(result_buffer, xo_therm_usr_format, &temperature->xo_therm_usr)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"xo_therm_usr = %d \n", temperature->xo_therm_usr);
            continue;
        }
        if(sscanf(result_buffer, sdx_case_therm_usr_format, &temperature->sdx_case_therm_usr)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"sdx_case_therm_usr = %d \n", temperature->sdx_case_therm_usr);
            continue;
        }
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

int32_t m5gm_sim_get_pin_protection(void)
{
    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+CLCK=\"SC\",2", "-t", AT_COMMAND_TIMEOUT, NULL};
    const char *iccid_format = "+CLCK: %d";
    int32_t pin_protection = -1;
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);

        if(sscanf(result_buffer, iccid_format, &pin_protection)) {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"pin_protection = %d \n", pin_protection);
            continue;
        }
        if(pin_protection != -1 && strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = pin_protection;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_sim_set_pin_protection(u_int32_t enable, const char * pin)
{
    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char at_command_buffer[128] = {0};
    FILE* fp;
    int32_t pid = 0;
    if(atoi(pin) > 99999999 || atoi(pin) < 0)
        return -2;
    if(enable > 1)
        return -3;
    
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    snprintf(at_command_buffer, sizeof(at_command_buffer), "AT+CLCK=\"SC\",%d,\"%s\"", enable, pin);
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", at_command_buffer, "-t", AT_COMMAND_TIMEOUT, NULL};
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_sim_get_pin_retry(void)
{

    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t pin_retries_left = -1;
    int32_t puk_retries_left = -1;
    const char *format = "+QPINC: \"SC\",%d,%d";
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+QPINC=\"SC\"", "-t", AT_COMMAND_TIMEOUT, NULL};
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(sscanf(result_buffer, format, &pin_retries_left, &puk_retries_left)) {
            
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"pin_retries_left = %d \n", pin_retries_left);
            continue;
        }
        if(pin_retries_left != -1 && strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = pin_retries_left;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_sim_set_pin_code(const char *  old_pin, const char *  new_pin)
{
    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char at_command_buffer[128] = {0};
    FILE* fp;
    int32_t pid = 0;
    if(atoi(old_pin) > 99999999 || atoi(new_pin) > 99999999 || atoi(old_pin) < 0 || atoi(new_pin) < 0)
        return -3;
    //not working if pin protection is disable.
    if(m5gm_sim_get_pin_protection() == 0)
        return -2;
    
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    snprintf(at_command_buffer, sizeof(at_command_buffer), "AT+CPWD=\"SC\",\"%s\",\"%s\"", old_pin, new_pin);
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", at_command_buffer, "-t", AT_COMMAND_TIMEOUT, NULL};
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_sim_unlock_pin(const char * pin)
{
    //not working if password mode is ready.

    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char at_command_buffer[128] = {0};
    FILE* fp;
    int32_t pid = 0;
    snprintf(at_command_buffer, sizeof(at_command_buffer), "AT+CPIN=\"%s\"", pin);
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", at_command_buffer, "-t", AT_COMMAND_TIMEOUT, NULL};
    
    if(atoi(pin) > 99999999 || atoi(pin) < 0)
        return -2;
    
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_sim_pin_status(void)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t mode = PIN_PASSWORD_MODE_UNKNOWN;
    int32_t ret = -1;
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+CPIN?", "-t", AT_COMMAND_TIMEOUT, NULL};
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strstr(result_buffer, "+CPIN: READY")!=NULL) {
            mode = PIN_PASSWORD_MODE_READY;
            continue;
        }
        else if(strstr(result_buffer, "+CPIN: SIM PIN2")!=NULL) {
            mode = PIN_PASSWORD_MODE_SIM_PIN2;
            continue;
        }
        else if(strstr(result_buffer, "+CPIN: SIM PUK2")!=NULL) {
            mode = PIN_PASSWORD_MODE_SIM_PUK2;
            continue;
        }
        else if(strstr(result_buffer, "+CPIN: SIM PIN")!=NULL) {
            mode = PIN_PASSWORD_MODE_SIM_PIN;
            continue;
        }
        else if(strstr(result_buffer, "+CPIN: SIM PUK")!=NULL) {
            mode = PIN_PASSWORD_MODE_SIM_PUK;
            continue;
        }
        if(mode != PIN_PASSWORD_MODE_UNKNOWN && strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = mode;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

int32_t m5gm_set_ps_attach_state(u_int32_t state)
{

    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char at_command[124] = {0};
    FILE* fp;
    int32_t pid = 0;
    if(state > CARRIER_ATTACH)
        return -2;
    if(state == CARRIER_DETACH)
    {
        //force release connection first
        m5gm_conn_stop(0);
        //force release client id to prevent client id leaks.
        m5gm_release_qmi_all_client_id();
    }
    snprintf(at_command, sizeof(at_command), "AT+CGATT=%d", state);
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", at_command, "-t", AT_COMMAND_TIMEOUT, NULL};
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);

    if(ret != 0)
    {
        //try again
        fp = moxa_popen(command_list, "r", &pid);
        while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
            M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
            if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
                ret = 0;
                break;
            }
        }
        waitpid(pid, NULL, 0);
        pclose(fp);
    }
    
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_manual_operator_selection(u_int32_t mode, u_int32_t operator_no, u_int32_t access_technology)
{

    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char at_command[124] = {0};
    FILE* fp;
    int32_t pid = 0;

    if(mode > OP_SELECT_MANUAL)
        return -2;
    if(access_technology > OP_ACCESS_NGRAN)
        return -3;
    snprintf(at_command, sizeof(at_command), "AT+COPS=%d,2,\"%d\",%d", mode, operator_no, access_technology);
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", at_command, "-t", AT_COMMAND_TIMEOUT, NULL};
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_set_network_search_mode(struct network_search_mode mode)
{

    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char at_command[124] = {0};
    FILE* fp;
    int32_t pid = 0;

    if(mode.is_auto)
        snprintf(at_command, sizeof(at_command), "AT+QNWPREFCFG=\"mode_pref\",AUTO");
    else if(mode.wcdma || mode.lte || mode.nr5g)
    {
        snprintf(at_command, sizeof(at_command), "AT+QNWPREFCFG=\"mode_pref\",");
        if(mode.wcdma)
            strcat(at_command, ":WCDMA");
        if(mode.lte)
            strcat(at_command, ":LTE");
        if(mode.nr5g)
            strcat(at_command, ":NR5G");
    }
    else
        return -2;
    
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", at_command, "-t", AT_COMMAND_TIMEOUT, NULL};
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_get_network_search_mode(struct network_search_mode * mode)
{

    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    if(!mode)
        return -2;
    memset(mode, 0, sizeof(struct network_search_mode));
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+QNWPREFCFG=\"mode_pref\"", "-t", AT_COMMAND_TIMEOUT, NULL};
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strstr(result_buffer, "AUTO")!=NULL) {
            mode->is_auto = 1;
        }
        if(strstr(result_buffer, "WCDMA")!=NULL) {
            mode->wcdma = 1;
        }
        if(strstr(result_buffer, "LTE")!=NULL) {
            mode->lte = 1;
        }
        if(strstr(result_buffer, "NR5G")!=NULL) {
            mode->nr5g = 1;
        }
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t _m5gm_get_network_registration_status(struct network_registration_status * status, u_int32_t lock_enable)
{

    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t status_get = 0;
    if(!status)
        return -2;
    memset(status, 0, sizeof(struct network_registration_status));
    
    const char *format = "+CREG: %d,%d,\"%[^\"]\",\"%[^\"]\",%d";
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+CREG?", "-t", AT_COMMAND_TIMEOUT, NULL};
    int32_t fd = 0;
    if(lock_enable)
    {
        fd = M5GM_LOCK();
        if(fd < 0)
            return M5GM_FAIL_TO_LOCK;
    }
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(sscanf(result_buffer, format, &status->unsolicited_result_code, 
            &status->registration_status,
            status->lac,
            status->ci,
            &status->act)) 
        {
            status_get = 1;
            continue;
        }
        if(status_get && strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    if(lock_enable)
        M5GM_UNLOCK(fd);
    return ret;
}

int32_t m5gm_get_network_registration_status(struct network_registration_status * status)
{
    return _m5gm_get_network_registration_status(status, 1);
}

int32_t m5gm_module_scan_site(struct neighbour_cell_status * cell_status)
{
    int32_t ret = -1;
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t index = 0;
    struct network_registration_status network_status;
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+QENG=\"neighbourcell\"", "-t", AT_COMMAND_TIMEOUT, NULL};
    
    const char *lte_neighbour_intra_format = "+QENG: \"neighbourcell intra\",\"LTE\",%*[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,]";
    const char *lte_neighbour_inter_format = "+QENG: \"neighbourcell inter\",\"LTE\",%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%*[^,],%*[^,],%*[^,],%*[^,]";
    const char *lte_neighbour_wcdma_format = "+QENG: \"neighbourcell\",\"WCDMA\",%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%[^,],%[^,],%*[^,]";
    const char *wcdma_neighbour_wcdma_format = "+QENG: \"neighbourcell\",\"WCDMA\",%*[^,],%*[^,],%*[^,],%[^,],%[^,],%*[^,],%*[^,],%*[^,]";
    const char *wcdma_neighbour_lte_format = "+QENG: \"neighbourcell\",\"LTE\",%*[^,],%[^,],%[^,],%[^,],%*[^,]";

    char cellid[MAX_NAME_LEN] = {0};
    
    char lte_rssi[MAX_NAME_LEN] = {0};
    char lte_rsrq[MAX_NAME_LEN] = {0};
    char lte_rsrp[MAX_NAME_LEN] = {0};
    char lte_snr[MAX_NAME_LEN] = {0};
    
    char wcdma_rssi[MAX_NAME_LEN] = {0};
    char wcdma_ecio[MAX_NAME_LEN] = {0};

    //fp = fopen("./test/output5","w+");
    //fprintf(fp, "+QENG: \"neighbourcell intra\",\"LTE\",38950,276,-3,-88,-65,0,37,7,16,6,44\r\n");
    //fprintf(fp, "+QENG: \"neighbourcell inter\",\"LTE\",39148,-,-,-,-,-,37,0,30,7,-,-,-,-\r\n");
    //fprintf(fp, "+QENG: \"neighbourcell inter\",\"LTE\",37900,-,-,-,-,-,0,0,30,6,-,-,-,-\r\n");
    //fprintf(fp, "OK\r\n");
    //fseek(fp, 0, SEEK_SET);

    if(!cell_status)
        return -2;
    
    int32_t lock = M5GM_LOCK();
    if(lock < 0)
        return M5GM_FAIL_TO_LOCK;
    
    if(_m5gm_get_network_registration_status(&network_status, 0))
    {
        M5GM_UNLOCK(lock);
        return -3;
    }  
    
    if (network_status.act >= OP_ACCESS_NGRAN)
    {
        //5g mode
        M5GM_UNLOCK(lock);
        return -4;
    }
    fp = moxa_popen(command_list, "r", &pid);
    if (network_status.act < OP_ACCESS_NGRAN && network_status.act >= OP_ACCESS_EUTRAN)
    { //LTE mode
        while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
            M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
            if(index >= MAX_NEIGHBOUR_CELL)
                continue;
            if(sscanf(result_buffer, lte_neighbour_intra_format,
                cellid,
                lte_rsrq,
                lte_rsrp,
                lte_rssi,
                lte_snr)) 
            {
                cell_status->cell[index].cellid = atoi(cellid);
                cell_status->cell[index].lte_rsrq = atoi(lte_rsrq);
                cell_status->cell[index].lte_rsrp = atoi(lte_rsrp);
                cell_status->cell[index].lte_rssi = atoi(lte_rssi);
                cell_status->cell[index].lte_snr = (float) atoi(lte_snr);
                cell_status->cell[index].is_lte = 1;
                index++;
                continue;
            }
            if(sscanf(result_buffer, lte_neighbour_inter_format,
                cellid,
                lte_rsrq,
                lte_rsrp,
                lte_rssi,
                lte_snr)) 
            {
                cell_status->cell[index].cellid = atoi(cellid);
                cell_status->cell[index].lte_rsrq = atoi(lte_rsrq);
                cell_status->cell[index].lte_rsrp = atoi(lte_rsrp);
                cell_status->cell[index].lte_rssi = atoi(lte_rssi);
                cell_status->cell[index].lte_snr = (float) atoi(lte_snr);
                cell_status->cell[index].is_lte = 1;
                index++;
                continue;
            }
            if(sscanf(result_buffer, lte_neighbour_wcdma_format,
                wcdma_rssi,
                wcdma_ecio)) 
            {
                cell_status->cell[index].wcdma_rssi = atoi(wcdma_rssi);
                cell_status->cell[index].wcdma_ecio = (float) atoi(wcdma_ecio);
                cell_status->cell[index].is_wcdma = 1;
                index++;
                continue;
            }
            if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
                ret = index;
                break;
            }
        }
    }
    else //WCDMA mode
    {
        while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
            M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
            if(index >= 20)
                continue;
            if(sscanf(result_buffer, wcdma_neighbour_lte_format,
                cellid,
                lte_rsrq,
                lte_rsrp)) 
            {
                cell_status->cell[index].cellid = atoi(cellid);
                cell_status->cell[index].lte_rsrq = atoi(lte_rsrq);
                cell_status->cell[index].lte_rsrp = atoi(lte_rsrp);
                cell_status->cell[index].is_lte = 1;
                index++;
                continue;
            }
            if(sscanf(result_buffer, wcdma_neighbour_wcdma_format,
                wcdma_rssi,
                wcdma_ecio)) 
            {
                cell_status->cell[index].wcdma_rssi = atoi(wcdma_rssi);
                cell_status->cell[index].wcdma_ecio = (float) atoi(wcdma_ecio);
                cell_status->cell[index].is_wcdma = 1;
                index++;
                continue;
            }
            if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
                ret = index;
                break;
            }
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(lock);
    return ret;
}

int32_t m5gm_set_module_functionality(u_int32_t functionality, u_int32_t reset)
{
    char at_command[124] = {0};

    if(functionality == 3 || functionality > 4)
        return -1;
    if(reset >= 2)
        return -2;
    if(reset == 1 && functionality != 1)
        return -3;
    snprintf(at_command, sizeof(at_command), "AT+CFUN=%d,%d", functionality, reset);

    return m5gm_op_at(at_command, 20000);
}

int32_t m5gm_get_module_functionality(void)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    int32_t functionality = -1;
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", "AT+CFUN?", "-t", AT_COMMAND_TIMEOUT, NULL};
    const char *format = "+CFUN: %d";
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(sscanf(result_buffer, format, &functionality)) 
        {
            continue;
        }
        if(functionality != -1 && strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = functionality;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

u_int32_t m5gm_module_get_fw_upgrade_progress()
{
    u_int32_t ret = 0;
    ret = upgrade_percent;
    if(upgrade_percent >= 100)
    {
        upgrade_percent = 0;
    }
    return ret;
}

int32_t m5gm_module_fw_upgrade(const char *fw_path)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char percent_str[MAX_NAME_LEN] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    const char * const command_list[] = {QFIREHOSE_PATH, "-f", fw_path, NULL};
    const char *format = "%*s upgrade progress %[^%]";
    if(!fw_path)
    {
        return -2;
    }
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    upgrade_percent = 0;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 300) != NULL) {
        M5GM_DEBUG(M5GM_LOG_DEBUG ,"%s\n",result_buffer);
        if(sscanf(result_buffer, format, percent_str)) 
        {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"percent_str: %s\n",percent_str);
            upgrade_percent = (u_int32_t)atoi(percent_str);
            continue;
        }
        if(strstr(result_buffer, QFIREHOSE_COMMAND_SUCCESS) != 0) {
            ret = 0;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;
}

int32_t _m5gm_get_band(const char * network_mode, struct band_config *band_config)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char band_string[512] = {0};
    char at_command[512] = {0};
    char format[512] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    u_int32_t is_band_set = 0;
    snprintf(at_command, sizeof(at_command), "AT+QNWPREFCFG=\"%s\"", network_mode);
    snprintf(format, sizeof(format), "+QNWPREFCFG: \"%s\",%%s", network_mode);
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", at_command, "-t", AT_COMMAND_TIMEOUT, NULL};
    
    if(!band_config)
        return -2;
    
    if(!network_mode)
        return -3;
    
    memset(band_config, 0, sizeof(struct band_config));
    
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(sscanf(result_buffer, format, band_string)) 
        {
            M5GM_DEBUG(M5GM_LOG_TRACE ,"band_string = %s\n", band_string);
            char *str = band_string;
            char *end = band_string;
            while(*end)
            {
                u_int32_t n = (u_int32_t)strtol(str, &end, 10);
                M5GM_DEBUG(M5GM_LOG_TRACE ,"band %d\n", n);
                band_config->band[n] = 1;
                
                while (*end == ':') 
                {
                    end++;
                }
                str = end;
            }
            is_band_set = 1;
            continue;
            
        }
        if(is_band_set && strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

int32_t m5gm_get_wcdma_band(struct band_config *band_config)
{
    return _m5gm_get_band("gw_band", band_config);
}

int32_t m5gm_get_lte_band(struct band_config *band_config)
{
    return _m5gm_get_band("lte_band", band_config);
}

int32_t m5gm_get_nr5g_band(struct band_config *band_config)
{
    return _m5gm_get_band("nr5g_band", band_config);
}

int32_t _m5gm_set_band(const char * network_mode, struct band_config band_config)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char at_command[2048] = {0};
    char band_str[32] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;
    u_int32_t i = 0;

    if(!network_mode)
        return -2;
    
    snprintf(at_command, sizeof(at_command), "AT+QNWPREFCFG=\"%s\",", network_mode);
    
    for(i = 0; i < MAX_BAND_NUM; i++)
    {
        if(band_config.band[i])
        {
            snprintf(band_str, sizeof(band_str), ":%d", i);
            strcat(at_command, band_str);
        }
    }
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", at_command, "-t", AT_COMMAND_TIMEOUT, NULL};
    
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    M5GM_UNLOCK(fd);
    return ret;

}

int32_t m5gm_set_wcdma_band(struct band_config band_config)
{
    return _m5gm_set_band("gw_band", band_config);
}

int32_t m5gm_set_lte_band(struct band_config band_config)
{
    return _m5gm_set_band("lte_band", band_config);
}

int32_t m5gm_set_nr5g_band(struct band_config band_config)
{
    return _m5gm_set_band("nr5g_band", band_config);
}

int32_t m5gm_set_nsa_nr5g_band(struct band_config band_config)
{
    return _m5gm_set_band("nsa_nr5g_band", band_config);
}

int32_t m5gm_module_power_down_no_lock(u_int32_t mode)
{
    char result_buffer[MAX_RESULT_BUFFER_LEN] = {0};
    char at_command[2048] = {0};
    FILE* fp;
    int32_t pid = 0;
    int32_t ret = -1;

    if(mode > 1)
        return -2;
    
    snprintf(at_command, sizeof(at_command), "AT+QPOWD=%d", mode);
    const char * const command_list[] = {MXAT_PATH, "-d", AT_DEV, "-c", at_command, "-t", AT_COMMAND_TIMEOUT, NULL};
   
    fp = moxa_popen(command_list, "r", &pid);
    while(fgets_timeout(result_buffer, sizeof(result_buffer), fp, 20) != NULL) {
        M5GM_DEBUG(M5GM_LOG_TRACE ,"\n%s %d  result_buffer=%s \n",__func__,__LINE__,result_buffer);
        if(strncmp(result_buffer, AT_COMMAND_SUCCESS, strlen(result_buffer)) == 0) {
            ret = 0;
            break;
        }
    }
    waitpid(pid, NULL, 0);
    pclose(fp);
    return ret;

}


int32_t _m5gm_gpio_out_set_value(const char* gpio_pin, const u_int32_t value, const u_int32_t delay_msec)
{
    int32_t ret = -1;
    
    if(value > 255)
        return -1;
    
    if(!gpio_pin)
        return -2;
    
    ret = M5GM_SYSTEM_WITH_LOG("echo %d > %s/%s/brightness", value, GPIO_SYS_CLASS_PATH, gpio_pin);
    if(ret)
    {
        return -3;
    }
    
    usleep(delay_msec * 1000);
    return 0;
}

int32_t _m5gm_gpio_get_value(const char* gpio_pin)
{
    FILE* fp = NULL;
    char gpio_path[256] = {0};
    char buffer[256] = {0};
    
    if(!gpio_pin)
        return -1;
    
    snprintf(gpio_path, sizeof(gpio_path), "%s/%s/brightness", GPIO_SYS_CLASS_PATH, gpio_pin);
    
    fp = moxa_fopen(gpio_path, "r");
    
    if(fp == NULL)
        return -2;
    
    if(fread(buffer, 1, sizeof(buffer), fp) <= 0)
    {
        fclose(fp);
        return -3;
    }
    
    fclose(fp);
    return atoi(buffer);
}

int32_t m5gm_module_set_power_on(void)
{
    int32_t ret = -1;
    int32_t full_card_power_off = -1;
    int32_t power_control = -1;
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    full_card_power_off = _m5gm_gpio_get_value(GPIO_FULL_CARD_POWER_OFF);
    power_control = _m5gm_gpio_get_value(GPIO_5G_PWR_CONTROL);
    
    if(full_card_power_off > GPIO_VALUE_LOW && power_control > GPIO_VALUE_LOW)
    {
        M5GM_UNLOCK(fd);
        return 0;
    }
    
    ret = _m5gm_gpio_out_set_value(GPIO_5G_PWR_CONTROL, GPIO_VALUE_HI, 100);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -2;
    }
    ret = _m5gm_gpio_out_set_value(GPIO_FULL_CARD_POWER_OFF, GPIO_VALUE_HI, 100);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -3;
    }
    M5GM_UNLOCK(fd);
    return 0;
    
}

int32_t m5gm_module_set_power_off(void)
{
    int32_t ret = -1;
    int32_t full_card_power_off = -1;
    int32_t power_control = -1;
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    full_card_power_off = _m5gm_gpio_get_value(GPIO_FULL_CARD_POWER_OFF);
    power_control = _m5gm_gpio_get_value(GPIO_5G_PWR_CONTROL);

    if(full_card_power_off == GPIO_VALUE_LOW && power_control == GPIO_VALUE_LOW)
    {
        M5GM_UNLOCK(fd);
        return 0;
    }
    ret = m5gm_module_power_down_no_lock(IMMEDIATE_POWER_DOWN);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -1;
    }

    ret = _m5gm_gpio_out_set_value(GPIO_FULL_CARD_POWER_OFF, GPIO_VALUE_LOW, 10000);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -2;
    }
    ret = _m5gm_gpio_out_set_value(GPIO_5G_PWR_CONTROL, GPIO_VALUE_LOW, 0);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -3;
    }
    M5GM_UNLOCK(fd);
    return 0;
    
}

int32_t m5gm_module_set_power_cycle(void)
{
    int32_t ret = -1;
    int32_t full_card_power_off = -1;
    int32_t power_control = -1;
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    full_card_power_off = _m5gm_gpio_get_value(GPIO_FULL_CARD_POWER_OFF);
    power_control = _m5gm_gpio_get_value(GPIO_5G_PWR_CONTROL);
    if(!(full_card_power_off == GPIO_VALUE_HI && power_control == GPIO_VALUE_HI))
    {
        M5GM_UNLOCK(fd);
        return -1;
    }
    ret = _m5gm_gpio_out_set_value(GPIO_RESET, GPIO_VALUE_LOW, 600);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -2;
    }
    ret = _m5gm_gpio_out_set_value(GPIO_RESET, GPIO_VALUE_HI, 0);
    if(ret)
    {
        M5GM_UNLOCK(fd);
        return -3;
    }
    M5GM_UNLOCK(fd);
    return 0;
    
}

int32_t m5gm_module_get_power_status(void)
{
    int32_t ret = -1;
    int32_t reset = -1;
    int32_t full_card_power_off = -1;
    int32_t power_control = -1;
    int32_t fd = M5GM_LOCK();
    if(fd < 0)
        return M5GM_FAIL_TO_LOCK;
    
    reset = _m5gm_gpio_get_value(GPIO_RESET);
    M5GM_DEBUG(M5GM_LOG_TRACE ,"reset = %d\n", reset);
    full_card_power_off = _m5gm_gpio_get_value(GPIO_FULL_CARD_POWER_OFF);
    M5GM_DEBUG(M5GM_LOG_TRACE ,"full_card_power_off = %d\n", full_card_power_off);
    power_control = _m5gm_gpio_get_value(GPIO_5G_PWR_CONTROL);
    M5GM_DEBUG(M5GM_LOG_TRACE ,"power_control = %d\n", power_control);
    
    if(reset < GPIO_VALUE_LOW || full_card_power_off < GPIO_VALUE_LOW || power_control < GPIO_VALUE_LOW)
    {
        M5GM_UNLOCK(fd);
        return -2;
    }
    
    if(reset == GPIO_VALUE_LOW)
        ret = CELLULAR_MODULE_POWER_CYCLE;
    else if(full_card_power_off == GPIO_VALUE_LOW && power_control == GPIO_VALUE_LOW)
        ret = CELLULAR_MODULE_POWER_OFF;
    else if(full_card_power_off > GPIO_VALUE_LOW && power_control > GPIO_VALUE_LOW)
        ret = CELLULAR_MODULE_POWER_ON;
    else
        ret = CELLULAR_MODULE_POWER_STATUS_UNKNOWN;

    M5GM_UNLOCK(fd);
    return ret;
    
}

