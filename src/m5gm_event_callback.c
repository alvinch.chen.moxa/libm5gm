/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <unistd.h>
#include <pthread.h>

#include "lib_m5gm/lib_m5gm.h"

struct event_callback_pair event_callback_table[MAX_CALLBACK_NUM] = {0};

pthread_t event_monitor_thread_t;

int32_t event_monitor_thread_run;



void * _event_monitor_thread(void *arg) 
{
    int32_t index = 0;
    pthread_t event_callback_thread_t;

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED); 

    while(event_monitor_thread_run)
    {
        index = 0;
        for (index = 0; index < MAX_CALLBACK_NUM; index++)
        {
            if(event_callback_table[index].event_detect_fun && event_callback_table[index].callback_fun)
            {
                M5GM_DEBUG(M5GM_LOG_TRACE, "\n%s %d  index = %d \n",__func__,__LINE__,index);
                if(event_callback_table[index].event_detect_fun(event_callback_table[index].arg_ptr))
                    pthread_create(&event_callback_thread_t, &attr, _event_callback_thread, (void *)&event_callback_table[index]);
            }
            //check if the thread shall keep running.
            if(!event_monitor_thread_run)
            {
                pthread_exit(NULL);
                break;
            }
        }
        sleep(1);
    };
    pthread_exit(NULL);
    return NULL;
}

void * _event_callback_thread(void *arg) 
{
    struct event_callback_pair * pair = (struct event_callback_pair * ) arg; 
    pair->callback_fun(pair->arg_ptr);
    pthread_exit(NULL);
}

int32_t m5gm_event_monitor_start(void)
{
    int32_t ret;
    event_monitor_thread_run = 1;
    ret = pthread_create(&event_monitor_thread_t, NULL, _event_monitor_thread, NULL);
    return ret;
}
    
int32_t m5gm_event_monitor_stop(void)
{
    int32_t ret = -1;
    if(event_monitor_thread_run == 0)
        return 0;
    event_monitor_thread_run = 0;
    ret = pthread_join(event_monitor_thread_t, NULL);
    return ret;
}

int32_t m5gm_event_callback_register(u_int32_t arg_length, int32_t (*event_detect_fun)(void *), int32_t (*callback_fun)(void *)) 
{
    int32_t index = 0;
    if(!event_detect_fun || !callback_fun)
        return -1;
    
    for (index = 0; index < MAX_CALLBACK_NUM; index++)
    {
        if(event_callback_table[index].event_detect_fun == event_detect_fun)
        {
            M5GM_DEBUG(M5GM_LOG_DEBUG , "\n%s %d  index = %d \n",__func__,__LINE__,index);
            
            event_callback_table[index].callback_fun = callback_fun;
            return 0;
            
        }
        else if(event_callback_table[index].event_detect_fun == NULL)
        {
            M5GM_DEBUG(M5GM_LOG_DEBUG ,"\n%s %d  index = %d \n",__func__,__LINE__,index);
            event_callback_table[index].event_detect_fun = event_detect_fun;
            event_callback_table[index].callback_fun = callback_fun;
            if(arg_length)
            {
                event_callback_table[index].arg_ptr = malloc(arg_length);
                memset(event_callback_table[index].arg_ptr, 0, arg_length);
            }
            else
            {
                event_callback_table[index].arg_ptr = NULL;
            }
            return 0;
        }
    }
    return -1;
}

int32_t m5gm_connection_status_change_event_detect_fun(void *arg)
{
    int32_t ret = 0;
    int32_t current_stat;
    static int32_t prev_stat = 0;
    int * save_status = (int *) arg;
    //printf("m5gm_connection_status_change_event_detect_fun\n");
    current_stat = m5gm_conn_get_status();
    if(current_stat == -1)
        return 0;

    ret = (prev_stat != current_stat);
    prev_stat = current_stat;
    //printf("# prev = %d new = %d \n",prev_stat,current_stat);
    if(ret)
    {
        //save prev status to status[1]
        save_status[M5GM_EVENT_OLD_STATUS_IDX] = save_status[M5GM_EVENT_NEW_STATUS_IDX];
        //save current status to status[1]
        save_status[M5GM_EVENT_NEW_STATUS_IDX] = current_stat;
    }
    
    return ret;
}

int32_t m5gm_signal_status_change_event_detect_fun(void *arg)
{
    int32_t ret = 0;
    //printf("m5gm_signal_status_change_event_detect_fun\n");
    
    static struct signal_status prev_stat = {0};
    struct signal_status current_stat = {0};
    struct signal_status * save_status = (struct signal_status *)arg;
    
    if(m5gm_get_signal_status(&current_stat) == -1)
        return 0;
    ret = (current_stat.is_5g != prev_stat.is_5g || current_stat.is_lte != prev_stat.is_lte || current_stat.is_wcdma != prev_stat.is_wcdma);
    //printf("# old status wcdma %d lte %d 5g %d\n", prev_stat.is_wcdma, prev_stat.is_lte, prev_stat.is_5g);
    //printf("# new status wcdma %d lte %d 5g %d\n", current_stat.is_wcdma, current_stat.is_lte, current_stat.is_5g);
    memcpy(&prev_stat , &current_stat, sizeof(struct signal_status));
    if(ret)
    {
        //save prev status to status[0]
        memcpy(&save_status[M5GM_EVENT_OLD_STATUS_IDX] , &save_status[M5GM_EVENT_NEW_STATUS_IDX], sizeof(struct signal_status));
        //save current status to status[1]
        memcpy(&save_status[M5GM_EVENT_NEW_STATUS_IDX] , &current_stat, sizeof(struct signal_status));
    }
    return ret;
}

int32_t m5gm_operational_mode_change_event_detect_fun(void *arg)
{
    int32_t ret = 0;
    int32_t current_stat;
    static int32_t prev_stat = 0;
    int * save_status = (int *) arg;
    printf("m5gm_operational_mode_change_event_detect_fun\n");
    current_stat = m5gm_get_operational_mode();
    printf("# prev = %d new = %d \n",prev_stat,current_stat);
    if(current_stat == -1)
        return 0;
    
    ret = (prev_stat != current_stat);
    prev_stat = current_stat;
    if(ret)
    {
        //save prev status to status[1]
        save_status[M5GM_EVENT_OLD_STATUS_IDX] = save_status[M5GM_EVENT_NEW_STATUS_IDX];
        //save current status to status[1]
        save_status[M5GM_EVENT_NEW_STATUS_IDX] = current_stat;

    }
    
    return ret;
}

int32_t m5gm_attach_status_change_event_detect_fun(void *arg)
{
    int32_t ret = 0;
    int32_t current_stat;
    static int32_t prev_stat = 0;
    int * save_status = (int *) arg;
    
    current_stat = m5gm_get_attach_status();
    if(current_stat == -1)
        return 0;
    ret = (prev_stat != current_stat);
    prev_stat = current_stat;
    if(ret)
    {
        //save prev status to status[1]
        save_status[M5GM_EVENT_OLD_STATUS_IDX] = save_status[M5GM_EVENT_NEW_STATUS_IDX];
        //save current status to status[1]
        save_status[M5GM_EVENT_NEW_STATUS_IDX] = current_stat;
    }
    return ret;

}


