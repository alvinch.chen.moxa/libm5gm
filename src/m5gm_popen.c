/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <unistd.h>
#include "lib_m5gm/lib_m5gm.h"

int32_t print_popen_command(const char * const argv[])
{
    size_t buffer_len = 2048 -1;
    char command_buffer[2048] = {0};
    int32_t index = 0;
    if(!argv)
        return -1;
    while(argv[index])
    {
        strncat(command_buffer, argv[index], buffer_len - 1);
        buffer_len = buffer_len - strlen(argv[index]);
        strncat(command_buffer, " ", buffer_len - 1);
        buffer_len--;
        index++;
    }
    M5GM_DEBUG(M5GM_LOG_INFO , command_buffer, NULL);
    return 0;
}

FILE * moxa_popen(const char * const argv[], const char *type, pid_t * pid) {

    int32_t fds[2];
    
    if(!argv || !type || !pid)
        return NULL;
    print_popen_command(argv);
    
    pipe(fds);
    
    if ((*pid = fork()) == 0) {
        
        close(fds[0]);
        dup2(fds[1], STDOUT_FILENO);
        dup2(fds[1], STDERR_FILENO);
        close(fds[1]);
        execvp(argv[0], (char * const*)argv);
        exit(-1);
    }
    close(fds[1]);
    return fdopen(fds[0], type);
}

char * fgets_timeout(char *result_buffer, int32_t n, FILE *stream, u_int32_t second)
{
    fd_set set;
    struct timeval timeout;
    int32_t ret = 0;
    if(!result_buffer || !stream || !n)
        return NULL;
    
    /* Initialize the file descriptor set. */
    FD_ZERO(&set);
    FD_SET(fileno(stream), &set);
    /* Initialize the timeout data structure. */
    timeout.tv_sec = second;
    timeout.tv_usec = 0;
    
    memset(result_buffer, 0, (size_t)n);

    ret = select(fileno(stream)+1, &set, NULL, NULL, &timeout);


    if (ret == 0) {
        return NULL;
    } else if (ret < 0) {
        return NULL;
    } else {
        return fgets(result_buffer, n, stream);
    }
}

FILE * moxa_fopen(const char *filename, const char *mode)
{
    return fopen(filename, mode);
}

