/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */
#include "lib_m5gm/lib_m5gm.h"
#include <stdlib.h>

u_int32_t current_log_level = M5GM_LOG_ERROR;

// debug mesg

int32_t default_logging(u_int32_t level, const char * msg)
{
    return 0;
}

int32_t (*logging_func_ptr)(u_int32_t , const char * ) = default_logging;

int32_t m5gm_logging_func_register(int32_t (*logging_func)(u_int32_t , const char * )) 
{
    if(!logging_func)
        return -1;

    M5GM_DEBUG(M5GM_LOG_DEBUG ,"\n%s %d  \n",__func__,__LINE__);
    logging_func_ptr = logging_func;
    return 0;
}

int32_t m5gm_set_log_level(u_int32_t level)
{
    if (level > M5GM_LOG_OFF)
        return -1;
    current_log_level = level;
    return 0;
}

