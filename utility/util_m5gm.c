#include "lib_m5gm/lib_m5gm.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <pthread.h>

int32_t active_trigger_event = 0;

struct signal_callback_result
{
    struct signal_status previus;
    struct signal_status current;
};

static void _printUsage(const char *aExecName)
{
    printf("Usage: %s a:pou:tch:dgm:z:x:rnqv: \n", aExecName);
    printf("Arguments:\n");
    printf("  -a: TEST START connection with apn\n");
    printf("  -p: Test START connection without apn\n");
    printf("  -o: Test STOP connection\n");
    printf("  -u: Test AT COMMAND \n");
    printf("  -t: Demo Status Get APIs\n");
    printf("  -c: Demo Event Monitor Thread \n");
    printf("  -g: Demo Event Monitor Thread 2 \n");
    printf("  -d: Demo Active Trigger Event \n");
    printf("  -h: Test Pin Protection \n");
    printf("  -m: Set PS attach state  \n");
    printf("  -z: Manual operator selection to 3g  \n");
    printf("  -x: Manual operator selection to 4g \n");
    printf("  -r: Reboot module via ATCMD \n");
    printf("  -w: Set network search mode to 5G \n");
    printf("  -e: Set network search mode to 5G \n");
    printf("  -f: Test conn start x 1000 times  \n");
    printf("  -s: Test conn start+stop x 1000 times \n");
    printf("  -j: Upgrade Firmware \n");
    printf("  -n: Test band set and get \n");
    printf("  -q: Test g_link thread \n");
    printf("  -v: Test module power down \n");
    printf("  -b: Test module power functions \n");
    printf("  -l: Test flight mode \n");
}

int32_t test_logging_func(u_int32_t level, const char * msg)
{
    printf("[test_logging_func] %s \n",msg);
    return 0;
}

void * upgrade_progress_thread(void *arg) 
{
    u_int32_t percent = 0;
    while(1)
    {
        percent = m5gm_module_get_fw_upgrade_progress();
        printf("upgrade progress: [%d%]\n", percent);
        
        if(percent == 100)
            break;
        sleep(1);
    }
    pthread_exit(NULL);
}


int32_t example_signal_event_detect_fun(void *arg)
{
    int32_t ret = 0;
    static struct signal_status prev_stat = {0};
    struct signal_callback_result * result = (struct signal_callback_result *) arg;
    struct signal_status current_stat = {0};
    if(ret = m5gm_get_signal_status(&current_stat) != 0)
    {
        printf("example_signal_event_detect_fun m5gm_get_signal_status ret= %d\n", ret);
        return 0;
    }
    ret = memcmp(&current_stat, &prev_stat, sizeof(struct signal_status));
    if(ret)
    {
        result->previus = prev_stat;
        result->current = current_stat;
    }
    prev_stat = current_stat;
    return ret;
}

int32_t example_signal_event_callback_fun(void * arg)
{
    // event_call_back shall be thread safe! (must).
    // Shall not run forever!
    struct signal_callback_result * result = (struct signal_callback_result *) arg;
    int32_t fd =M5GM_LOCK();
    printf("\n%s %d EVENT signal_status changed 1 \n",__func__,__LINE__);
    //demo a very slow callback.
    printf("is_5g = %d rsrp_5g = %d, snr_5g = %f \n",result->current.is_5g, result->current.rsrp_5g, result->current.snr_5g);
    printf("is_lte = %d lte_rsrq = %d, lte_snr = %f \n",result->current.is_lte, result->current.lte_rsrq, result->current.lte_snr);
    printf("is_wcdma = %d rssi = %d, ecio = %f \n",result->current.is_wcdma, result->current.wcdma_rssi, result->current.wcdma_ecio);
    sleep(3);
    printf("\n%s %d EVENT signal_status changed 2 \n",__func__,__LINE__);
    M5GM_UNLOCK(fd);
    return 0;
}

int32_t example_conn_status_event_detect_fun(void * arg)
{
    int32_t ret = 0;
    int32_t current_stat;
    static int32_t prev_stat = 0;
    current_stat = m5gm_conn_get_status();
    ret = (prev_stat != current_stat);
    *(int32_t*)arg = current_stat;
    prev_stat = current_stat;
    return ret;
}

int32_t example_conn_status_event_callback_fun(void * arg)
{
    //event_call_back shall be thread safe! (must)
    printf("\n%s %d EVENT Connection changed to %d \n",__func__,__LINE__, * (int32_t*) arg);
    return 0;
}

int32_t example_active_trigger_event_detect_fun(void *arg)
{
    int32_t ret = 0;
    if(active_trigger_event)
    {
        active_trigger_event--;
        ret = 1;
    }
    return ret;
}

int32_t example_active_trigger_event_callback_fun(void * arg)
{
    //event_call_back shall be thread safe! (must)
    printf("\n%s %d EVENT triggered by user or hardware actively. \n",__func__,__LINE__);
    return 0;
}

int32_t main(int32_t argc, char * const argv[])
{
    const char *apn = NULL;
    const char *fw_path = NULL;
    const char *at_command = NULL;
    int32_t ret = 0;
    int32_t opt;
    int32_t state;
    int32_t operator;
    const char * pin = NULL;
    u_int32_t i = 0;
    char carrier_name[200] = {0};
    char imei[200] = {0};
    char status[200] = {0};
    pthread_attr_t attr;
    pthread_t upgrade_progress_thread_t;
    struct band_config band_config = {0};
    struct signal_status stat = {0};
    struct module_info info = {0};
    struct sim_info sim_info = {0};
    struct module_temperature temperature = {255,255,255,255,255,255,255,255,255,255};
    struct carrier_info carrier = {0};
    struct network_search_mode mode = {0};
    struct network_registration_status reg_status = {0};
    struct neighbour_cell_status cell_status = {0};
    struct g_link_thread_config config = {0};
    m5gm_set_log_level(M5GM_LOG_DEBUG);
    
    m5gm_logging_func_register(test_logging_func);
    
    while ((opt = getopt(argc, argv, "a:pou:tch:dgm:z:x:werfsj:nqv:b:l:")) != -1) {
        printf("command = %c\n", opt);
        switch (opt) {
        case 'a':
            //start connection with apn
            apn = optarg;
            ret = m5gm_conn_start(apn, 0);
            printf("m5gm_conn_start(%s) = %d \n",apn, ret);
            return 0;
        case 'p':
            //start connection without apn
            ret = m5gm_conn_start(NULL, 0);
            printf("m5gm_conn_start(%s) = %d \n",NULL, ret);
            return 0;
        case 'o':
            // stop connection
            ret = m5gm_conn_stop(0);
            printf("m5gm_conn_stop = %d \n", ret);
            return 0;
        case 'u':
            // AT command
            at_command = optarg;
            ret = m5gm_op_at(at_command, 10000);
            printf("m5gm_op_at = %d \n", ret);
            return 0;
        case 't':
            //status test
            printf("m5gm_get_operational_mode = %d\n", m5gm_get_operational_mode());
            ret = m5gm_conn_lte_tracking_area_code();
            printf("m5gm_conn_lte_tracking_area_code = %d \n", ret);
            ret = m5gm_conn_get_status();
            printf("m5gm_conn_get_status = %d \n", ret);
            ret = m5gm_get_attach_status();
            printf("m5gm_get_attach_status = %d \n", ret);
            ret = m5gm_get_signal_status(&stat);
            printf("m5gm_get_signal_status = %d \n", ret);
            printf("cellid = %d \n", stat.cellid);
            printf("is_5g = %d rsrp_5g = %d, snr_5g = %f \n",stat.is_5g, stat.rsrp_5g, stat.snr_5g);
            printf("is_lte = %d lte_rsrq = %d, lte_snr = %f \n",stat.is_lte, stat.lte_rsrq, stat.lte_snr);
            printf("is_wcdma = %d rssi = %d, ecio = %f \n",stat.is_wcdma, stat.wcdma_rssi, stat.wcdma_ecio);
            
            ret = m5gm_get_carrier_info(&carrier);
            printf("m5gm_get_carrier_info = %d \n", ret);
            
            printf("service_provider_full_name = %s \n", carrier.service_provider_full_name);
            printf("service_provider_short_name = %s \n", carrier.service_provider_short_name);
            printf("registered_plmn = %s \n", carrier.registered_plmn);
            ret = m5gm_module_get_info(&info);
            printf("m5gm_module_get_info = %d \n", ret);
            printf("manufacturer = %s module_name = %s, firmware_version = %s \n",info.manufacturer, info.module_name, info.firmware_version);
            ret = m5gm_get_imei(imei);
            printf("m5gm_get_imei = %d \n", ret);
            printf("imei = %s \n", imei);
            ret = m5gm_sim_get_info(&sim_info);
            printf("m5gm_sim_get_info = %d \n", ret);
            printf("imsi = %s iccid = %s\n",sim_info.imsi, sim_info.iccid);
            ret = m5gm_module_get_temp(&temperature);
            printf("m5gm_module_get_temp = %d \n", ret);
            printf("qfe_wtr_pa0 = %d aoss0_usr = %d, mdm_q6_usr = %d \n",temperature.qfe_wtr_pa0, temperature.aoss0_usr, temperature.mdm_q6_usr);
            printf("ipa_usr = %d cpu0_a7_usr = %d, mdm_5g_usr = %d \n",temperature.ipa_usr, temperature.cpu0_a7_usr, temperature.mdm_5g_usr);
            printf("mdm_core_usr = %d mdm_vpe_usr = %d, xo_therm_usr = %d \n",temperature.mdm_core_usr, temperature.mdm_vpe_usr, temperature.xo_therm_usr);
            printf("sdx_case_therm_usr = %d \n",temperature.sdx_case_therm_usr);
            ret = m5gm_sim_get_pin_protection();
            printf("m5gm_sim_get_pin_protection = %d \n", ret);
            ret = m5gm_sim_get_pin_retry();
            printf("m5gm_sim_get_pin_retry = %d \n", ret);
            ret = m5gm_sim_pin_status();
            printf("m5gm_sim_pin_status = %d \n", ret);
            ret = m5gm_get_network_search_mode(&mode);
            printf("m5gm_get_network_search_mode() = %d \n", ret);
            printf("is_auto = %d wcdma = %d, lte = %d, nr5g = %d \n",mode.is_auto, mode.wcdma, mode.lte,mode.nr5g);
            ret = m5gm_get_network_registration_status(&reg_status);
            printf("m5gm_get_network_registration_status() = %d \n", ret);
            printf("unsolicited_result_code = %d registration_status = %d, lac = %s, ci = %s, act =%d \n",reg_status.unsolicited_result_code, reg_status.registration_status, reg_status.lac,reg_status.ci, reg_status.act);
            ret = m5gm_module_scan_site(&cell_status);
            printf("m5gm_module_scan_site() = %d \n", ret);
            if(ret == 0)
                for(i = 0; i< ret; i++)
                {
                    printf("neighbour cell [%d]:\n", i);
                    if(cell_status.cell[i].is_5g)
                    {
                        printf("cellid = %d\n", cell_status.cell[i].cellid);
                        printf("is_5g = %d rsrp_5g = %d, snr_5g = %f \n",cell_status.cell[i].is_5g, cell_status.cell[i].rsrp_5g, cell_status.cell[i].snr_5g);
                    }
                    if(cell_status.cell[i].is_lte)
                    {
                        printf("cellid = %d\n", cell_status.cell[i].cellid);
                        printf("is_lte = %d lte_rsrq = %d, lte_snr = %f \n",cell_status.cell[i].is_lte, cell_status.cell[i].lte_rsrq, cell_status.cell[i].lte_snr);
                    }
                    if(cell_status.cell[i].is_wcdma)
                        printf("is_wcdma = %d rssi = %d, ecio = %f \n",cell_status.cell[i].is_wcdma, cell_status.cell[i].wcdma_rssi, cell_status.cell[i].wcdma_ecio);
                }
            ret = m5gm_get_module_functionality();
            printf("m5gm_get_module_functionality() = %d \n", ret);
            ret = m5gm_get_wcdma_band(&band_config);
            printf("m5gm_get_wcdma_band() = %d \n", ret);

            for(i=0; i < MAX_BAND_NUM; i++)
            {
                if(band_config.band[i])
                    printf("wcdma band_[%d] = 1 \n",i);
            }
            memset(&band_config, 0, sizeof(struct band_config));
            ret = m5gm_get_lte_band(&band_config);
            printf("m5gm_get_lte_band() = %d \n", ret);
            for(i=0; i < MAX_BAND_NUM; i++)
            {
                if(band_config.band[i])
                    printf("lte band_[%d] = 1 \n",i);
            }
            memset(&band_config, 0, sizeof(struct band_config));
            ret = m5gm_get_nr5g_band(&band_config);
            printf("m5gm_get_nr5g_band() = %d \n", ret);
            for(i=0; i < MAX_BAND_NUM; i++)
            {
                if(band_config.band[i])
                    printf("nr5g band_[%d] = 1 \n",i);
            }
            ret = m5gm_g_link_ping("8.8.8.8", 1);
            printf("m5gm_g_link_ping() = %d \n", ret);
            return 0;
        case 'c':
            //event monitor test
            
            m5gm_event_callback_register(sizeof(struct signal_callback_result), example_signal_event_detect_fun, example_signal_event_callback_fun);
            m5gm_event_callback_register(sizeof(int32_t), example_conn_status_event_detect_fun, example_conn_status_event_callback_fun);
            ret = m5gm_event_monitor_start();
            printf("m5gm_event_monitor_start = %d \n", ret);
            sleep(10);
            ret = m5gm_event_monitor_stop();
            printf("m5gm_event_monitor_stop = %d \n", ret);
            return 0;
            break;
        case 'd':
            //active event triggered  test
            
            m5gm_event_callback_register(0, example_active_trigger_event_detect_fun, example_active_trigger_event_callback_fun);
            ret = m5gm_event_monitor_start();
            printf("m5gm_event_monitor_start = %d \n", ret);
            sleep(5);
            active_trigger_event++;
            sleep(5);
            active_trigger_event += 2;
            sleep(5);
            ret = m5gm_event_monitor_stop();
            printf("m5gm_event_monitor_stop = %d \n", ret);
            return 0;
            break;
        case 'h':
            // Test Pin Protection
            pin = optarg;
            if(m5gm_sim_pin_status() == PIN_PASSWORD_MODE_SIM_PIN)
            {
                ret = m5gm_sim_unlock_pin(pin);
                printf("m5gm_sim_unlock_pin  = %d \n", ret);
            }
            
            if(! m5gm_sim_get_pin_protection())
            {
                ret = m5gm_sim_set_pin_protection( 1, pin);
                printf("m5gm_sim_set_pin_protection enable = %d \n", ret);
            }

            if(ret != 0)
            {
                printf("pin error!!\n");
                return -1;
            }
            ret = m5gm_sim_set_pin_code(pin, "1111");
            printf("m5gm_sim_set_pin_protection enable = %d \n", ret);
            ret = m5gm_sim_set_pin_protection( 0, "1111");
            printf("m5gm_sim_set_pin_protection disable = %d \n", ret);
            ret = m5gm_sim_set_pin_protection( 1, "1111");
            printf("m5gm_sim_set_pin_protection enable = %d \n", ret);
            ret = m5gm_sim_set_pin_code("1111", pin);
            printf("m5gm_sim_set_pin_protection enable = %d \n", ret);
            ret = m5gm_sim_set_pin_protection( 0, pin);
            printf("m5gm_sim_set_pin_protection disable = %d \n", ret);
            return 0;
        case 'g':
            m5gm_event_callback_register(0, example_conn_status_event_detect_fun, example_conn_status_event_callback_fun);
            m5gm_event_monitor_start();
            sleep(5);
            m5gm_conn_start("internet", 0);
            sleep(5);
            m5gm_conn_stop(0);
            sleep(5);
            m5gm_conn_start("internet", 0);
            sleep(5);
            m5gm_conn_stop(0);
            sleep(5);
            m5gm_conn_start("internet", 0);
            sleep(5);
            m5gm_conn_stop(0);
            sleep(5);
            m5gm_event_monitor_stop();
            sleep(5);
            m5gm_conn_start("internet", 0);
            sleep(5);
            m5gm_conn_stop(0);
            sleep(5);
            return 0;
        case 'm':
            state = atoi(optarg);
            ret = m5gm_set_ps_attach_state(state);
            printf("m5gm_set_ps_attach_state(%d) = %d \n", state, ret);
            return 0;
        case 'z':
            operator = atoi(optarg);
            ret = m5gm_manual_operator_selection(OP_SELECT_MANUAL, operator, OP_ACCESS_UTRAN);
            printf("m5gm_manual_operator_selection(1,%d,2) = %d \n", operator, ret);
            return 0;
        case 'x':
            operator = atoi(optarg);
            ret = m5gm_manual_operator_selection(OP_SELECT_MANUAL, operator, OP_ACCESS_EUTRAN);
            printf("m5gm_manual_operator_selection(1, %d,7) = %d \n", operator, ret);
            return 0;
        case 'w':
            mode.is_auto = 0;
            mode.wcdma = 0;
            mode.lte = 0;
            mode.nr5g = 1;
            ret = m5gm_set_network_search_mode(mode);
            printf("m5gm_set_network_search_mode() = %d \n", ret);
            return 0;
        case 'e':
            mode.is_auto = 1;
            mode.wcdma = 0;
            mode.lte = 0;
            mode.nr5g = 0;
            ret = m5gm_set_network_search_mode(mode);
            printf("m5gm_set_network_search_mode() = %d \n", ret);
            return 0;
        case 'r':
            ret = m5gm_set_module_functionality(FULL_FUNCTIONALITY, RESET_UE);
            printf("m5gm_set_module_functionality() = %d \n", ret);
            return 0;
        case 'f':
            for(i = 0; i < 1000; i++)
            {
                ret = m5gm_conn_start("internet", 0);
                printf("(%d) m5gm_conn_start() = %d \n",i, apn, ret);
            }
            return 0;
        case 's':
            for(i = 0; i < 1000; i++)
            {
                ret = m5gm_conn_start("internet", 0);
                printf("(%d) m5gm_conn_start = %d \n", i, ret);
                ret = m5gm_conn_stop(0);
                printf("(%d) m5gm_conn_stop = %d \n", i, ret);
            }
            return 0;
        case 'j':
            fw_path = optarg;
            pthread_attr_init(&attr);
            pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED); 
            ret = pthread_create(&upgrade_progress_thread_t, &attr, upgrade_progress_thread, NULL);
            printf("pthread_create() = %d \n", ret);
            ret = m5gm_module_fw_upgrade(fw_path);
            printf("m5gm_module_fw_upgrade(%s) = %d \n",fw_path, ret);
            return 0;
        case 'n':
            ret = m5gm_get_wcdma_band(&band_config);
            printf("m5gm_get_wcdma_band() = %d \n", ret);

            for(i=0; i < MAX_BAND_NUM; i++)
            {
                if(band_config.band[i])
                    printf("wcdma band_[%d] = 1 \n",i);
            }
            band_config.band[7] = 1;
            ret = m5gm_set_wcdma_band(band_config);
            printf("m5gm_set_wcdma_band() = %d \n", ret);
            
            memset(&band_config, 0, sizeof(struct band_config));
            ret = m5gm_get_wcdma_band(&band_config);
            printf("m5gm_get_wcdma_band() = %d \n", ret);

            for(i=0; i < MAX_BAND_NUM; i++)
            {
                if(band_config.band[i])
                    printf("wcdma band_[%d] = 1 \n",i);
            }
            
            memset(&band_config, 0, sizeof(struct band_config));
            ret = m5gm_get_lte_band(&band_config);
            printf("m5gm_get_lte_band() = %d \n", ret);
            for(i=0; i < MAX_BAND_NUM; i++)
            {
                if(band_config.band[i])
                    printf("lte band_[%d] = 1 \n",i);
            }
            band_config.band[255] = 1;
            ret = m5gm_set_lte_band(band_config);
            printf("m5gm_set_lte_band() = %d \n", ret);
            
            memset(&band_config, 0, sizeof(struct band_config));
            ret = m5gm_get_lte_band(&band_config);
            printf("m5gm_get_lte_band() = %d \n", ret);

            for(i=0; i < MAX_BAND_NUM; i++)
            {
                if(band_config.band[i])
                    printf("wcdma band_[%d] = 1 \n",i);
            }

            memset(&band_config, 0, sizeof(struct band_config));
            ret = m5gm_get_nr5g_band(&band_config);
            printf("m5gm_get_nr5g_band() = %d \n", ret);
            for(i=0; i < MAX_BAND_NUM; i++)
            {
                if(band_config.band[i])
                    printf("nr5g band_[%d] = 1 \n",i);
            }
            band_config.band[261] = 1;
            ret = m5gm_set_nr5g_band(band_config);
            printf("m5gm_set_nr5g_band() = %d \n", ret);
            
            memset(&band_config, 0, sizeof(struct band_config));
            ret = m5gm_get_nr5g_band(&band_config);
            printf("m5gm_get_nr5g_band() = %d \n", ret);

            for(i=0; i < MAX_BAND_NUM; i++)
            {
                if(band_config.band[i])
                    printf("nr5g band_[%d] = 1 \n",i);
            }
            return 0;
        case 'q':
            config.connection_reset_threhold = 1;
            config.functional_reset_threhold = 2;
            config.ue_reset_threhold = 3;
            config.hard_reset_threhold = 4;
            config.ping_timeout = 2;
            sprintf(config.ping_host, "%s", "8.8.8.8");
            
            m5gm_set_log_level(M5GM_LOG_WARN);
            m5gm_g_link_set_apn_name("internet");
            m5gm_g_link_connection_start();
            ret = m5gm_g_link_thread_start(config);
            printf("m5gm_g_link_thread_start = %d \n", ret);
            sleep(120);
            ret = m5gm_conn_get_status();
            printf("m5gm_conn_get_status = %d \n", ret);
            
            m5gm_conn_stop(0);
            printf("manually stop connection! \n");
            sleep(5);
            ret = m5gm_conn_get_status();
            printf("m5gm_conn_get_status = %d \n", ret);

            m5gm_conn_stop(0);
            printf("manually stop connection! \n");
            sleep(5);
            ret = m5gm_conn_get_status();
            printf("m5gm_conn_get_status = %d \n", ret);
            
            m5gm_conn_stop(0);
            printf("manually stop connection! \n");
            sleep(5);
            ret = m5gm_conn_get_status();
            printf("m5gm_conn_get_status = %d \n", ret);
            
            m5gm_g_link_connection_stop();
            sleep(5);
            ret = m5gm_conn_get_status();
            printf("m5gm_conn_get_status = %d \n", ret);
            
            ret = m5gm_g_link_thread_stop();
            printf("m5gm_g_link_thread_stop = %d \n", ret);
            return 0;
        case 'v':
            state = atoi(optarg);
            ret = m5gm_module_power_down_no_lock(state);
            printf("m5gm_module_power_down_no_lock(%d) = %d \n", state, ret);
            return 0;
        case 'b':
            state = M5GM_UNLOCK(-1);
            printf("option %d\n", state);
            state = atoi(optarg);
            printf("option %d\n", state);
            printf("m5gm_module_get_power_status = %d\n", m5gm_module_get_power_status());
            printf("m5gm_get_operational_mode = %d\n", m5gm_get_operational_mode());
            if(state == 1)
            {
                ret = m5gm_module_set_power_on();
                printf("m5gm_module_set_power_on() = %d \n", ret);
            }
            if(state == 2)
            {
                ret = m5gm_module_set_power_off();
                printf("m5gm_module_set_power_off() = %d \n", ret);
            }
            if(state == 3)
            {
                ret = m5gm_module_set_power_cycle();
                printf("m5gm_module_set_power_cycle() = %d \n", ret);
            }
            return 0;
        case 'l':
            
            state = atoi(optarg);
            printf("option %d\n", state);
            printf("m5gm_set_flight_mode = %d\n", m5gm_set_flight_mode(state));
            if(state == 3)
                for(i = 0; i < 100; i++)
                {
                    ret = m5gm_conn_start("internet", 0);
                    printf("(%d) m5gm_conn_start = %d \n", i, ret);
                    sleep(5);
                    ret = m5gm_set_flight_mode(1);
                    printf("(%d) m5gm_set_flight_mode 1 = %d \n", i, ret);
                    ret = m5gm_set_flight_mode(0);
                    printf("(%d) m5gm_set_flight_mode 0 = %d \n", i, ret);
                    sleep(5);
                }
            
            return 0;


        default:
            _printUsage(argv[0]);
            return -1;
        }
    }
    _printUsage(argv[0]);
    return 0;
}
