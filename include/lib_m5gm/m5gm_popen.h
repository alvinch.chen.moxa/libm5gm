/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */

#ifndef LIB_M5GM_INCLUDE_LIB_M5GM_M5GM_POPEN_H
#define LIB_M5GM_INCLUDE_LIB_M5GM_M5GM_POPEN_H

#include <stdio.h>

FILE * moxa_popen(const char * const argv[], const char *type, pid_t * pid);
char * fgets_timeout(char *result_buffer, int32_t n, FILE *stream, u_int32_t second);
FILE * moxa_fopen(const char *filename, const char *mode);

#endif
