/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */

#ifndef LIB_M5GM_INCLUDE_LIB_M5GM_M5GM_G_LINK_H
#define LIB_M5GM_INCLUDE_LIB_M5GM_M5GM_G_LINK_H

struct g_link_thread_config
{
    u_int32_t connection_reset_threhold; // do connection reset if fail count reaches this threhold.
    u_int32_t functional_reset_threhold; // do functional reset if fail count reaches this threhold.
    u_int32_t ue_reset_threhold; // do UE reset if fail count reaches this threhold.
    u_int32_t hard_reset_threhold; // do hardware reset if fail count reaches this threhold. (TBD)
    char ping_host[512]; // ip or domain name
    u_int32_t ping_timeout; // 0 ~ 3 secs
};

int32_t m5gm_g_link_connection_start(void);
int32_t m5gm_g_link_connection_stop(void);
int32_t m5gm_g_link_set_apn_name(const char * name);
int32_t m5gm_g_link_thread_start(struct g_link_thread_config config);
int32_t m5gm_g_link_thread_stop(void);
int32_t m5gm_g_link_ping(const char * host, u_int32_t timeout);

#endif
