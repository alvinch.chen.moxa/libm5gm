/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */
#ifndef LIB_M5GM_INCLUDE_LIB_M5GM_M5GM_LOCK_H
#define LIB_M5GM_INCLUDE_LIB_M5GM_M5GM_LOCK_H

#define LIB_M5GM_LOCK_PATH "/tmp/lib_m5gm_lock"
#define M5GM_FAIL_TO_LOCK -9

#define M5GM_LOCK() ({int32_t retval; \
    M5GM_DEBUG(M5GM_LOG_TRACE,"[LOCK acquire] %s %d \n",__func__,__LINE__);\
    retval = m5gm_lock();\
    M5GM_DEBUG(M5GM_LOG_TRACE,"[LOCK acquired] %s %d FD = %d\n",__func__,__LINE__, retval);\
    retval;})

#define M5GM_UNLOCK(FD) ({int32_t retval2; \
    M5GM_DEBUG(M5GM_LOG_TRACE, "[LOCK release] %s %d FD = %d\n",__func__,__LINE__, FD); \
    retval2 = (FD < 0)?-1:m5gm_unlock(FD); \
    usleep(10 * 1000);\
    M5GM_DEBUG(M5GM_LOG_TRACE, "[LOCK released] %s %d \n",__func__,__LINE__); \
    retval2;})

int32_t m5gm_lock(void);
int32_t m5gm_unlock(int32_t);

#endif

