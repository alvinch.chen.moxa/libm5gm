/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */

#ifndef LIB_M5GM_INCLUDE_LIB_M5GM_M5GM_EVENT_CALLBACK_H
#define LIB_M5GM_INCLUDE_LIB_M5GM_M5GM_EVENT_CALLBACK_H

#define MAX_CALLBACK_NUM 128

#define M5GM_EVENT_NEW_STATUS_IDX 0
#define M5GM_EVENT_OLD_STATUS_IDX 1

struct event_callback_pair
{
    void * arg_ptr;
    int32_t (*event_detect_fun)(void *);
    int32_t (*callback_fun)(void *);
};


extern struct event_callback_pair event_callback_table[MAX_CALLBACK_NUM];
extern pthread_t event_monitor_thread_t;
extern int32_t event_monitor_thread_run;

void * _event_callback_thread(void *arg);

void * _event_monitor_thread(void *arg);

int32_t m5gm_event_monitor_start(void);

int32_t m5gm_event_monitor_stop(void);

int32_t m5gm_event_callback_register(u_int32_t arg_length, int32_t (*event_detect_fun)(void *), int32_t (*callback_fun)(void *));

int32_t m5gm_connection_status_change_event_detect_fun(void *arg);

int32_t m5gm_signal_status_change_event_detect_fun(void *arg);

int32_t m5gm_operational_mode_change_event_detect_fun(void *arg);

int32_t m5gm_attach_status_change_event_detect_fun(void *arg);

#endif
