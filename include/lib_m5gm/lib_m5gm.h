/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */
#ifndef LIB_M5GM_INCLUDE_LIB_M5GM_LIB_M5GM_H
#define LIB_M5GM_INCLUDE_LIB_M5GM_LIB_M5GM_H

#include <stdint.h>
#include <stdlib.h>
#include "lib_m5gm/m5gm_event_callback.h"
#include "lib_m5gm/m5gm_log.h"
#include "lib_m5gm/m5gm_popen.h"
#include "lib_m5gm/m5gm_lock.h"
#include "lib_m5gm/m5gm_g_link.h"

#define MAX_PATH_LEN 128
#define MAX_NAME_LEN 128
#define MAX_NEIGHBOUR_CELL 64
#define MAX_RESULT_BUFFER_LEN 2048
#define MAX_BAND_NUM 512

#define AT_COMMAND_SUCCESS "OK\r\n"
#define QFIREHOSE_COMMAND_SUCCESS "Upgrade module successfully"

#define GPIO_SYS_CLASS_PATH "/sys/class/leds"

#define GPIO_DIR_IN "in"
#define GPIO_DIR_OUT "out"

#define GPIO_VALUE_HI 255
#define GPIO_VALUE_LOW 0

#define GPIO_FULL_CARD_POWER_OFF "5G_en"
#define GPIO_5G_PWR_CONTROL "5G_pwr"
#define GPIO_RESET "5G_rst"

#define AT_COMMAND_TIMEOUT "10000"
#define CONN_STATE_FILE_PATH "/tmp/5g_conn_state"

extern char WWAN_DEV[MAX_PATH_LEN];
extern char QMICLI[MAX_PATH_LEN];
extern char QMI_NETWORK_PATH[MAX_PATH_LEN];
extern char M5GM_CONTROL_PATH[MAX_PATH_LEN];

extern char CDC_WDM[MAX_PATH_LEN];
extern char DHCPCD[MAX_PATH_LEN];
extern char IFCONFIG[MAX_PATH_LEN];
extern char AT_DEV[MAX_PATH_LEN];
extern char MXAT_PATH[MAX_PATH_LEN];

enum operating_mode {
    OP_MODE_ERROR = -1,
    OP_MODE_OFFLINE = 0,
    OP_MODE_ONLINE = 1,
    OP_MODE_RESET,
    OP_MODE_LOW_POWER = 4,
    OP_MODE_PERSISTENT_LOW_POWER,
};

enum pin_password_mode {
    PIN_PASSWORD_MODE_UNKNOWN = -1,
    PIN_PASSWORD_MODE_READY = 1,
    PIN_PASSWORD_MODE_SIM_PIN,
    PIN_PASSWORD_MODE_SIM_PUK,
    PIN_PASSWORD_MODE_SIM_PIN2,
    PIN_PASSWORD_MODE_SIM_PUK2,
};

enum op_select_mode {
    OP_SELECT_AUTO = 0,
    OP_SELECT_MANUAL = 1,
};

enum carrier_state {
    CARRIER_DETACH = 0,
    CARRIER_ATTACH = 1,
};

enum op_access_tech {
    OP_ACCESS_UTRAN = 2,
    OP_ACCESS_EUTRAN = 7,
    OP_ACCESS_NGRAN = 12,
};

enum module_functionality {
    MINIMUM_FUNCTIONALITY = 0,
    FULL_FUNCTIONALITY = 1,
    DISABLE_TX_RX = 4,
};

enum module_reset {
    DO_NOT_RESET = 0,
    RESET_UE = 1,
};

enum module_power_down_mode {
    IMMEDIATE_POWER_DOWN = 0,
    NORMAL_POWER_DOWN = 1,
};

enum module_power_status {
    CELLULAR_MODULE_POWER_OFF = 0,
    CELLULAR_MODULE_POWER_ON = 1,
    CELLULAR_MODULE_POWER_CYCLE = 2,
    CELLULAR_MODULE_POWER_STATUS_UNKNOWN = 3,
};

struct network_search_mode 
{
    u_int32_t is_auto;
    u_int32_t wcdma;
    u_int32_t lte;
    u_int32_t nr5g;
};

struct m5gm_path_config
{
    char WWAN_DEV[MAX_PATH_LEN];
    char QMICLI[MAX_PATH_LEN];
    char QMI_NETWORK_PATH[MAX_PATH_LEN];
    char M5GM_CONTROL_PATH[MAX_PATH_LEN];
    char CDC_WDM[MAX_PATH_LEN];
    char DHCPCD[MAX_PATH_LEN];
    char IFCONFIG[MAX_PATH_LEN];
    char AT_DEV[MAX_PATH_LEN];
    char MXAT_PATH[MAX_PATH_LEN];
    char QFIREHOSE_PATH[MAX_PATH_LEN];
};

struct module_info
{
    char manufacturer[MAX_NAME_LEN];
    char module_name[MAX_NAME_LEN];
    char firmware_version[MAX_NAME_LEN];
};

struct carrier_info
{
    char service_provider_full_name[MAX_NAME_LEN];
    char service_provider_short_name[MAX_NAME_LEN];
    char registered_plmn[MAX_NAME_LEN];
};

struct sim_info
{
    char imsi[MAX_NAME_LEN];
    char iccid[MAX_NAME_LEN];
};

struct signal_status
{
    int32_t cellid;
    int32_t is_5g;
    int32_t rsrp_5g;
    int32_t rsrq_5g;
    float snr_5g;
    int32_t is_lte; 
    int32_t lte_rssi;
    int32_t lte_rsrq;
    int32_t lte_rsrp;
    float lte_snr;
    int32_t is_wcdma;
    int32_t wcdma_rssi;
    float wcdma_ecio;
};

struct module_temperature
{
    int32_t qfe_wtr_pa0;
    int32_t aoss0_usr;
    int32_t mdm_q6_usr;
    int32_t ipa_usr;
    int32_t cpu0_a7_usr; 
    int32_t mdm_5g_usr;
    int32_t mdm_core_usr;
    int32_t mdm_vpe_usr;
    int32_t xo_therm_usr;
    int32_t sdx_case_therm_usr;
};

struct band_config
{
    u_int32_t band[MAX_BAND_NUM];
};

struct network_registration_status
{
    u_int32_t unsolicited_result_code;
    u_int32_t registration_status;
    char lac[MAX_NAME_LEN];
    char ci[MAX_NAME_LEN];
    u_int32_t act; 
};

struct neighbour_cell_status
{
    struct signal_status cell[MAX_NEIGHBOUR_CELL];
};

/**
 * @brief m5gm init function.
 *
 * @param[in]     apn       Object of m5gm_path_config.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_init(struct m5gm_path_config config);

/**
 * @brief Start Connection.
 *
 * @param[in]     apn       Apn name.
 * @param[in]     pdp       Profile index.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_conn_start(const char *apn, u_int32_t pdp);
/**
 * @brief Stop Connection.
 *
 * @param[in]     pdp       Profile index.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_conn_stop(u_int32_t pdp);
/**
 * @brief Get Connection Status.
 *
 * @returns                 1 if connected, 0 if disconnected, else if request failed.
 */
int32_t m5gm_conn_get_status(void);
/**
 * @brief Get LTE tracking area code.
 *
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_conn_lte_tracking_area_code(void);
/**
 * @brief Get Attach Status.
 *
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_attach_status(void);
/**
 * @brief Execute AT command.
 *
 * @param[in]     at_cmd    AT command string.
 * @param[in]     timeout   Command timeout in msec. 
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_op_at(const char *at_cmd, u_int32_t timeout);
/**
 * @brief Get Carrier info.
 *
 * @param[out]     info     Object of struct carrier_info.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_carrier_info(struct carrier_info * info);
/**
 * @brief Get Signal info.
 *
 * @param[out]     stat     Object of struct signal_status.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_signal_status(struct signal_status *stat);
/**
 * @brief Get Module info.
 *
 * @param[out]     info     Object of struct module_info.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_module_get_info(struct module_info * info);
/**
 * @brief Get IMEI.
 *
 * @param[out]     imei     String of imei.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_imei(char * imei);
/**
 * @brief Set Airplane mode.
 *
 * @param[in]      Enable   0 if disable, 1 if enable.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_set_flight_mode(u_int32_t enable);
/**
 * @brief Get functional mode.
 *
 * @returns                 1 if online, 2 if offline, 3 if reset, 4 if low-power, 5 if persist-low-power, -1 if request failed.
 */
int32_t m5gm_get_operational_mode(void);
/**
 * @brief Get Sim card info.
 *
 * @param[out]     info     Object of struct sim_info.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_sim_get_info(struct sim_info * info);
/**
 * @brief Get module temperature info.
 *
 * @param[out]     temperature     Object of struct module_temperature.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_module_get_temp(struct module_temperature * temperature);
/**
 * @brief Get pin protection.
 *
 * @returns                 1 if protection is on, 0 if protection is off, else if request failed.
 */
int32_t m5gm_sim_get_pin_protection(void);
/**
 * @brief Set pin protection.
 *
 * @param[in]      enable   0 if disable, 1 if enable.
 * @param[in]      pin      Pin code number.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_sim_set_pin_protection(u_int32_t enable, const char * pin);
/**
 * @brief Get pin retries number.
 *
 * @returns                 Pin retries number left, -1 if request failed.
 */
int32_t m5gm_sim_get_pin_retry(void);
/**
 * @brief Set pin protection.
 *
 * @param[in]      old_pin  Old pin code number.
 * @param[in]      new_pin  New pin code number.
 * @returns                 Retries number, -1 if request failed.
 */
int32_t m5gm_sim_set_pin_code(const char * old_pin, const char * new_pin);
/**
 * @brief Get pin password mode.
 *
 * @returns                 1 if ready, 2 if sim pin, 3 if sim puk, 4 if sim pin2, 5 if sim puk2, -1 if unknown or fail.
 */
int32_t m5gm_sim_pin_status(void);
/**
 * @brief unlock pin.
 *
 * @param[in]      pin      Pin code number.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_sim_unlock_pin(const char * pin);
/**
 * @brief set carrier attach state.
 *
 * @param[in]      state      1 if attach, 0 if detach.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_set_ps_attach_state(u_int32_t state);
/**
 * @brief manual operator selection.
 *
 * @param[in]      mode     0 if auto, 0 if manual.
 * @param[in]      operator_no     Numeric operator identification number
 * @param[in]      access_technology     2 if utran, 7 if e-utran, 12 if ng-ran
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_manual_operator_selection(u_int32_t mode, u_int32_t operator_no, u_int32_t access_technology);
/**
 * @brief Set network search mode.
 *
 * @param[in]      mode     Network search mode
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_set_network_search_mode(struct network_search_mode mode);
/**
 * @brief Get network search mode.
 *
 * @param[in]      mode     Network search mode
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_network_search_mode(struct network_search_mode * mode);
/**
 * @brief Get network registration status.
 *
 * @param[in]      status     Network registration status
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_network_registration_status(struct network_registration_status * status);
/**
 * @brief Get neighbour cell status.
 *
 * @param[in]      cell_status     Neighbour cell status
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_module_scan_site(struct neighbour_cell_status * cell_status);
/**
 * @brief Set module functionality.
 *
 * @param[in]      functionality     0 if minimum, 1 if full, 4 if disable tx and tx
 * @param[in]      reset     0 if do not reset, 1 if reset 
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_set_module_functionality(u_int32_t functionality, u_int32_t reset);
/**
 * @brief Get module functionality.
 *
 * @returns                 Module functionality, -1 if request failed.
 */
int32_t m5gm_get_module_functionality(void);
/**
 * @brief Upgrade module firmware.
 *
 * @param[in]      fw_path     firmware file path.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_module_fw_upgrade(const char *fw_path);
/**
 * @brief Get progress of module firmware upgrading.
 *
 * @returns                 0-100 when firmware upgrading, else if request failed.
 */
u_int32_t m5gm_module_get_fw_upgrade_progress(void);
/**
 * @brief Get WCDMA band setting.
 *
 * @param[in]      band_config     band setting
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_wcdma_band(struct band_config *band_config);
/**
 * @brief Get LTE band setting.
 *
 * @param[in]      band_config     band setting
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_lte_band(struct band_config *band_config);
/**
 * @brief Get NR5G band setting.
 *
 * @param[in]      band_config     band setting
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_nr5g_band(struct band_config *band_config);
/**
 * @brief Set WCDMA band setting.
 *
 * @param[in]      band_config     band setting
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_set_wcdma_band(struct band_config band_config);
/**
 * @brief Set LTE band setting.
 *
 * @param[in]      band_config     band setting
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_set_lte_band(struct band_config band_config);
/**
 * @brief Set NR5G band setting.
 *
 * @param[in]      band_config     band setting
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_set_nr5g_band(struct band_config band_config);
/**
 * @brief Set NSA NR5G band setting.
 *
 * @param[in]      band_config     band setting
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_set_nsa_nr5g_band(struct band_config band_config);
/**
 * @brief Set Module power on.
 *
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_module_set_power_on(void);
/**
 * @brief Set Module power off.
 *
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_module_set_power_off(void);
/**
 * @brief Set Module power cycle.
 *
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_module_set_power_cycle(void);
int32_t m5gm_module_power_down_no_lock(u_int32_t mode);
int32_t m5gm_module_get_power_status(void);
int32_t m5gm_release_qmi_all_client_id(void);
#endif