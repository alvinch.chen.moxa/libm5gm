/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file LICENSE for details.
 */
#ifndef LIB_M5GM_INCLUDE_LIB_M5GM_M5GM_LOG_H
#define LIB_M5GM_INCLUDE_LIB_M5GM_M5GM_LOG_H

extern int32_t (*logging_func_ptr)(u_int32_t , const char * );

extern u_int32_t current_log_level;

enum log_level {
    M5GM_LOG_ALL,
    M5GM_LOG_TRACE,
    M5GM_LOG_DEBUG,
    M5GM_LOG_INFO,
    M5GM_LOG_WARN,
    M5GM_LOG_ERROR,
    M5GM_LOG_FATAL,
    M5GM_LOG_OFF,
};

#define M5GM_DEBUG(level, fmt, ...) ({char log_buffer[4096] = {0}; \
                snprintf(log_buffer, 4096, fmt, __VA_ARGS__);\
                if(logging_func_ptr && level >= current_log_level) logging_func_ptr(level, log_buffer);\
                })

#define M5GM_SYSTEM_WITH_LOG(fmt, ...) ({int32_t retval; char command_buffer[4096] = {0}; \
                                    snprintf(command_buffer, 4096, fmt, __VA_ARGS__);\
                                    M5GM_DEBUG(M5GM_LOG_INFO, command_buffer, NULL);\
                                    retval = system(command_buffer);\
                                    retval;})

int32_t m5gm_set_log_level(u_int32_t level);

int32_t m5gm_logging_func_register(int32_t (*logging_func)(u_int32_t , const char * ));

#endif
