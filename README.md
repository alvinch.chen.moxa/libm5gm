# Lib m5gm (MOXA 5G Module)

A Library is implenmented to provide API for 5G module control and manipulation. 
### Main function includes:
- Start / Stop 5G connection
- Get 5G connection status
- Get module status
- Set 5G Operational Mode
- Set SIM pin 
- Event detect and callback 
- Log and debug
- Thread-safe
- A demo utility



### Event detection and handling:
Event detection and handling are supported by dynamic function registering.

Its flexible to add new event and handling function.

You can use following APIs to start to monitor event or stop monitoring event. 

```c
int32_t m5gm_event_callback_register(int32_t arg, int32_t (*event_detect_fun)(int32_t *), int32_t (*callback_fun)(int32_t *));
```
```c
int32_t m5gm_event_monitor_start(void);
```
```c
int32_t m5gm_event_monitor_stop(void);
```

`m5gm_event_callback_register` will register event detect function and event call-back function.

`m5gm_event_monitor_start` will create a monitor thread to execute event detect functions.

If a event is detected. 

Event monitor thread will create another thread to execute related event callback function.

Currently several event examples are implenmented in the demo utility.

`m5gm_event_monitor_stop` will stop the monitor thread.

### Example of event detect function calls

``` c
m5gm_event_callback_register(0, example_conn_status_event_detect_fun, example_conn_status_event_callback_fun);
m5gm_event_monitor_start();
sleep(5);
m5gm_conn_start("internet", 0);
sleep(5);
m5gm_conn_stop(0);
sleep(5);
m5gm_conn_start("internet", 0);
sleep(5);
m5gm_conn_stop(0);
sleep(5);
m5gm_conn_start("internet", 0);
sleep(5);
m5gm_conn_stop(0);
sleep(5);
m5gm_event_monitor_stop();
sleep(5);
m5gm_conn_start("internet", 0);
sleep(5);
m5gm_conn_stop(0);
sleep(5);
```
### Example code of event detection function of connection status change.

Event detection function returns `1` if event is detected.

Event detection function returns `0` if event is not detected.

argument `int32_t * arg`. can be passed between detection function and handling function.

In the example. It calls `m5gm_conn_status()` to get connection status and compare it the previous status.

And it returns `1` if connection status changes or return `0` if status not change.

It also saves the status to argument `arg`, so that the status can be passed to the event handling function.

``` c
int32_t example_conn_status_event_detect_fun(int32_t * arg)
{
    int32_t ret = 0;
    int32_t current_stat;
    static int32_t prev_stat = 0;
    current_stat = m5gm_conn_status();
    ret = (prev_stat != current_stat);
    *arg = current_stat;
    prev_stat = current_stat;
    return ret;
}
```

### Example code of event callback function

Event call_back function could be executed `simultaneously`.
It shall not run forever and it shall be thread safe.

```c
int32_t example_conn_status_event_callback_fun(int32_t * arg)
{
    //event_call_back shall be thread safe! (must)
    printf("\n%s %d EVENT Connection changed to %d \n",__func__,__LINE__, *arg);
    return 0;
}

```

### API list

```c

/**
 * @brief m5gm init function.
 *
 * @param[in]     apn       Object of m5gm_path_config.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_init(struct m5gm_path_config config);

/**
 * @brief Start Connection.
 *
 * @param[in]     apn       Apn name.
 * @param[in]     pdp       Profile index.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_conn_start(const char *apn, u_int32_t pdp);
/**
 * @brief Stop Connection.
 *
 * @param[in]     pdp       Profile index.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_conn_stop(u_int32_t pdp);
/**
 * @brief Get Connection Status.
 *
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_conn_status(void);
/**
 * @brief Get LTE tracking area code.
 *
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_conn_lte_tracking_area_code(void);
/**
 * @brief Get Attach Status.
 *
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_attach_status(void);
/**
 * @brief Execute AT command.
 *
 * @param[in]     at_cmd    AT command string.
 * @param[in]     timeout   Command timeout in msec. 
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_op_at(const char *at_cmd, u_int32_t timeout);
/**
 * @brief Get Carrier info.
 *
 * @param[out]     info     Object of struct carrier_info.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_carrier_info(struct carrier_info * info);
/**
 * @brief Get Signal info.
 *
 * @param[out]     stat     Object of struct signal_status.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_signal_status(struct signal_status *stat);
/**
 * @brief Get Module info.
 *
 * @param[out]     info     Object of struct module_info.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_module_info(struct module_info * info);
/**
 * @brief Get IMEI.
 *
 * @param[out]     imei     String of imei.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_get_imei(char * imei);
/**
 * @brief Set Airplane mode.
 *
 * @param[in]      Enable   0 if disable, 1 if enable.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_set_flight_mode(u_int32_t enable);
/**
 * @brief Get functional mode.
 *
 * @returns                 1 if online, 2 if offline, 3 if reset, 4 if low-power, 5 if persist-low-power, -1 if request failed.
 */
int32_t m5gm_get_functional_mode(void);
/**
 * @brief Get Sim card info.
 *
 * @param[out]     info     Object of struct sim_info.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_sim_get_info(struct sim_info * info);
/**
 * @brief Get module temperature info.
 *
 * @param[out]     temperature     Object of struct module_temperature.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_module_get_temp(struct module_temperature * temperature);
/**
 * @brief Get pin protection.
 *
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_sim_get_pin_protection(void);
/**
 * @brief Set pin protection.
 *
 * @param[in]      enable   0 if disable, 1 if enable.
 * @param[in]      pin      Pin code number.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_sim_set_pin_protection(u_int32_t enable, u_int32_t pin);
/**
 * @brief Get pin retries number.
 *
 * @returns                 Pin retries number left, -1 if request failed.
 */
int32_t m5gm_sim_get_pin_retry(void);
/**
 * @brief Set pin protection.
 *
 * @param[in]      old_pin  Old pin code number.
 * @param[in]      new_pin  New pin code number.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_sim_set_pin_code(u_int32_t old_pin, u_int32_t new_pin);
/**
 * @brief Get pin password mode.
 *
 * @returns                 1 if ready, 2 if sim pin, 3 if sim puk, 4 if sim pin2, 5 if sim puk2, -1 if unknown or fail.
 */
int32_t m5gm_sim_pin_status(void);
/**
 * @brief unlock pin.
 *
 * @param[in]      pin      Pin code number.
 * @returns                 0 if request success, else if request failed.
 */
int32_t m5gm_sim_unlock_pin(u_int32_t pin);
```

### Compile the library

```
* Build library

make -f build.makefile clean all

* Link as static library

gcc -o TARGET_FILE SRC_FILES libm5gm.a

* Link as dynamic library

gcc -o TARGET_FILE SRC_FILES libm5gm.so.1.0.0

* Run unittest

make -f cpputest.makefile
make -f unittest.makefile clean gcov
```
