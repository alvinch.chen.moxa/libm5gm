HOST=aarch64-marvell-linux
TARGET=
CC=gcc
PWD=$(shell pwd)
CPPUTEST_NAME=cpputest-3.8
CPPUTEST_PATH=$(PWD)/external
CPPUTEST_HOME=$(CPPUTEST_PATH)/$(CPPUTEST_NAME)

.PHONY: all
all:
ifneq ($(shell test -f ${CPPUTEST_HOME}/lib/libCppUTest.a; echo $$?),0)
	cd $(CPPUTEST_PATH); tar zxf $(CPPUTEST_PATH)/$(CPPUTEST_NAME).tar.gz
	cd $(CPPUTEST_HOME); \
		./autogen.sh ; \
		./configure --enable-coverage --build=$(HOST) CC=$(CC) ; \
		make ;
endif

clean:
	rm -rf $(CPPUTEST_HOME)
